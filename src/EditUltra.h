// #pragma once

#ifndef _H_EDITULTRA_
#define _H_EDITULTRA_

#include "framework.h"

extern char		**g_argv ;
extern int		g_argc ;
extern HINSTANCE	g_hAppInstance;
extern HWND		g_hwndMainWindow;
extern char		g_acAppName[100];

extern pcre		*pcreCppFunctionRe ;

extern char		g_acModuleFileName[ MAX_PATH ] ;
extern char		g_acModulePathName[ MAX_PATH ] ;

extern int		g_nZoomReset ;

void UpdateAllWindows();
void OnResizeWindow( HWND hWnd , int nWidth , int nHeight );

void PushOpenPathFilenameRecently( char *acPathFilename );
void UpdateOpenPathFilenameRecently();
void SetViewTabWidthMenuText( int nMenuId );
void SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( int nMenuId );
void UpdateAllMenus( HWND hWnd , struct TabPage *pnodeTabPage );

int BeforeWndProc( MSG *p_msg );
int AfterWndProc( MSG *p_msg );

#define CONFIG_KEY_MATERIAL_EDITULTRA	"EDITUL"

#endif
