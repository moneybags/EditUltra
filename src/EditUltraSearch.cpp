#include "framework.h"

struct list_head	listNavigateBackNextTrace ;
int			nMaxNavigateBackNextTraceCount = 0 ;

HWND			hwndSearchFind ;
HWND			hwndEditBoxInSearchFind ;
HWND			hwndSearchReplace ;
HWND			hwndFromEditBoxInSearchReplace ;
HWND			hwndToEditBoxInSearchReplace ;

void JumpGotoLine( struct TabPage *pnodeTabPage , int nGotoLineNo , int nCurrentLineNo )
{
	int nLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	int nScreenLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_LINESONSCREEN , 0 , 0 ) ;
	if( nGotoLineNo > nCurrentLineNo )
	{
		int nJumpLineNo = nGotoLineNo + nScreenLineCount / 2 ;
		if( nJumpLineNo > nLineCount )
			nJumpLineNo = nLineCount ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}
	else
	{
		int nJumpLineNo = nGotoLineNo - nScreenLineCount / 2 ;
		if( nJumpLineNo < 1 )
			nJumpLineNo = 1 ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}

	return;
}

static size_t PrepareSearchFlags( HWND hwndSearch )
{
	size_t		flags = 0 ;

	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WHOLEWORD ) )
		flags |= SCFIND_WHOLEWORD ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_MATCHCASE ) )
		flags |= SCFIND_MATCHCASE ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WORDSTART ) )
		flags |= SCFIND_WORDSTART ;

	if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_REGEXP ) )
		flags |= SCFIND_REGEXP ;
	else if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_POSIXREGEXP ) )
		flags |= SCFIND_POSIX ;

	return flags;
}

static int AdjustCurrentPosBeforeFindPrev()
{
	size_t nSelectStartPos=(size_t)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	size_t nSelectEndPos=(size_t)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETCURRENTPOS , nCurrentPos-1 , 0 ) ;
	}
	return nCurrentPos;
}

static int AdjustCurrentPosBeforeFindNext()
{
	int nSelectStartPos=(int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	int nSelectEndPos=(int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETCURRENTPOS , nCurrentPos+1 , 0 ) ;
	}

	return nCurrentPos;
}

static char *GetWindowString( HWND hWnd )
{
	int	nTextLen ;
	char	*pcText = NULL ;

	nTextLen = (int)::SendMessage( hWnd , WM_GETTEXTLENGTH , 0 , 0 ) ;
	if( nTextLen == 0 )
		return NULL;
	pcText = (char*)malloc( nTextLen+1) ;
	if( pcText == NULL )
		return NULL;
	memset( pcText , 0x00 , nTextLen+1 );
	::SendMessage( hWnd , WM_GETTEXT , nTextLen+1 , (LPARAM)pcText );

	return pcText;
}

static void SetWindowString( HWND hWnd , char *pcText )
{
	::SendMessage( hWnd , WM_SETTEXT , -1 , (LPARAM)pcText );
}

INT_PTR CALLBACK SearchFindWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDPREV )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				free( pcEditorSelectionText );
				break;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcFindDialogText ) == 0 ) )
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				nStartPos--;
				if( nStartPos < 0 )
					nStartPos = 0 ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcFindDialogText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			else
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcFindDialogText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDNEXT )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				free( pcEditorSelectionText );
				break;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcFindDialogText ) == 0 ) )
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
				nStartPos++;
				if( nStartPos > nEndPos )
					nStartPos = nEndPos ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			else
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SearchReplaceWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEPREV )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				free( pcEditorSelectionText );
				break;
			}
			char	*pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				free( pcReplaceDialogFromText );
				free( pcEditorSelectionText );
				break;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				nStartPos--;
				if( nStartPos < 0 )
					nStartPos = 0 ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFoundPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			else
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACENEXT )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				free( pcEditorSelectionText );
				break;
			}
			char	*pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				free( pcReplaceDialogFromText );
				free( pcEditorSelectionText );
				break;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
				nStartPos++;
				if( nStartPos > nEndPos )
					nStartPos = nEndPos ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}
			else
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEALL )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				free( pcEditorSelectionText );
				break;
			}
			char	*pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				free( pcReplaceDialogFromText );
				free( pcEditorSelectionText );
				break;
			}

			if( pcEditorSelectionText == NULL )
			{
				Sci_TextToFind	ft ;
				int	nStartPos = 0 ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				while(1)
				{
					ft.chrg.cpMin = nStartPos ;
					ft.chrg.cpMax = nEndPos ;
					ft.lpstrText = pcReplaceDialogFromText ;
					int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
					if( nFindPos >= 0 )
					{
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

						nStartPos = nFindPos + 1 ;
					}
					else
					{
						break;
					}
				}

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}
			else
			{
				Sci_TextToFind	ft ;
				int	nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int	nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				int	nFileEndPos = nEndPos ;
				int	nDeltaLen = (int)strlen(pcReplaceDialogToText) - (int)strlen(pcReplaceDialogFromText) ;
				if( nDeltaLen < 0 )
					nDeltaLen = 0 ;

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				while(1)
				{
					ft.chrg.cpMin = nStartPos ;
					ft.chrg.cpMax = nEndPos ;
					ft.lpstrText = pcReplaceDialogFromText ;
					int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
					if( nFindPos >= 0 )
					{
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

						nStartPos = nFindPos + 1 ;
						nEndPos += nDeltaLen ;
						if( nEndPos > nFileEndPos )
							nEndPos = nFileEndPos ;
					}
					else
					{
						break;
					}
				}

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

int OnSearchFind( struct TabPage *pnodeTabPage )
{
	if( g_pnodeCurrentTabPage )
	{
		CopyEditorSelectionToWnd( hwndEditBoxInSearchFind );

		::SetFocus( hwndEditBoxInSearchFind );

		CenterWindow( hwndSearchFind , g_hwndMainWindow );
		ShowWindow( hwndSearchFind , SW_SHOW );
	}

	return 0;
}

int OnSearchFindPrev( struct TabPage *pnodeTabPage )
{
	if( g_pnodeCurrentTabPage )
	{
		CopyEditorSelectionToWnd( hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDPREV , 0 );
	}

	return 0;
}

int OnSearchFindNext( struct TabPage *pnodeTabPage )
{
	if( g_pnodeCurrentTabPage )
	{
		CopyEditorSelectionToWnd( hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDNEXT , 0 );
	}

	return 0;
}

int OnSearchReplace( struct TabPage *pnodeTabPage )
{
	if( g_pnodeCurrentTabPage )
	{
		CopyEditorSelectionToWnd( hwndFromEditBoxInSearchReplace );

		::SendMessage( hwndSearchReplace , DM_SETDEFID , ID_REPLACENEXT , 0 );
		::SetFocus( hwndFromEditBoxInSearchReplace );

		CenterWindow( hwndSearchReplace , g_hwndMainWindow );
		ShowWindow( hwndSearchReplace , SW_SHOW );
	}

	return 0;
}

int OnSelectAll( struct TabPage *pnodeTabPage )
{
	if( g_pnodeCurrentTabPage )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SELECTALL, 0, 0 );
	}

	return 0;
}

int OnSelectWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentWordStartPos ;
		int	nCurrentWordEndPos ;

		nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentWordStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDSTARTPOSITION, nCurrentPos, true ) ;
		nCurrentWordEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDENDPOSITION, nCurrentPos, true ) ;
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETSEL, nCurrentWordStartPos, nCurrentWordEndPos ) ;
	}

	return 0;
}

int OnSelectLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nCurrentLineStartPos ;
		int	nCurrentLineEndPos ;
		int	nEolMode ;

		nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		nCurrentLineStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		nCurrentLineEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLINEENDPOSITION, nCurrentLine, 0 ) ;
		nEolMode = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETEOLMODE, 0, 0 ) ;
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETSEL, nCurrentLineStartPos, nCurrentLineEndPos+(nEolMode==0?2:1) ) ;
	}

	return 0;
}

int OnAddSelectLeftCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectLeftWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectTopBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARAUPEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectBottomBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARADOWNEXTEND, 0, 0 );
	return 0;
}

int OnMoveLeftCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTLEFT, 0, 0 );
	return 0;
}

int OnMoveRightCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTRIGHT, 0, 0 );
	return 0;
}

int OnMoveLeftWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDLEFT, 0, 0 );
	return 0;
}

int OnMoveRightWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDRIGHT, 0, 0 );
	return 0;
}

int OnMoveTopBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARAUP, 0, 0 );
	return 0;
}

int OnMoveBottomBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARADOWN, 0, 0 );
	return 0;
}

int OnSearchGotoLine( struct TabPage *pnodeTabPage )
{
	char		LineNo[ 20 ] ;

	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	memset( LineNo , 0x00 , sizeof(LineNo) );
	strcpy( LineNo , "0" );
	nret = InputBox( g_hwndMainWindow , "请输入行号：" , "输入窗口" , 0 , LineNo , sizeof(LineNo)-1 ) ;
	if( nret == IDOK )
	{
		int	nGotoLine ;
		int	nCurrentPos ;
		int	nCurrentLine ;

		nGotoLine = atoi(LineNo) ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		JumpGotoLine( pnodeTabPage , nGotoLine-1 , nCurrentLine );
	}

	return 0;
}

int OnSearchToggleBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nMarkNumber ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nMarkNumber = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERGET , nCurrentLine , 0 ) ;
	if( nMarkNumber == MARGIN_BOOKMARK_VALUE )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDELETE , nCurrentLine , MARGIN_BOOKMARK_MASKN );
	}
	else
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERADD , nCurrentLine , MARGIN_BOOKMARK_MASKN );
	}

	return 0;
}

int OnSearchAddBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERADD , nCurrentLine , MARGIN_BOOKMARK_MASKN );

	return 0;
}

int OnSearchRemoveBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDELETE , nCurrentLine , MARGIN_BOOKMARK_MASKN );

	return 0;
}

int OnSearchRemoveAllBookmarks( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_MARKERDELETEALL , MARGIN_BOOKMARK_MASKN , 0 );
	}

	return 0;
}

int OnSearchGotoPrevBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nFindLineNo = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , MARGIN_BOOKMARK_VALUE ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( pnodeTabPage , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoNextBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nFindLineNo = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , MARGIN_BOOKMARK_VALUE ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( pnodeTabPage , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoPrevBookmarkInAllFiles( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	int		nCurrentPos ;
	int		nCurrentLine ;
	int		nFindLineNo ;
	int		nMaxLine ;
	
	if( pnodeTabPage == NULL )
		return 0;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , MARGIN_BOOKMARK_VALUE ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{
				for( nTabPageIndex-- ; nTabPageIndex >= 0 ; nTabPageIndex-- )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);

					nMaxLine = (int)p->pfuncScintilla( p->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nMaxLine-1 , MARGIN_BOOKMARK_VALUE ) ;
					if( nFindLineNo != -1 )
					{
						SelectTabPageByIndex( nTabPageIndex );
						JumpGotoLine( p , nFindLineNo , nMaxLine );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

int OnSearchGotoNextBookmarkInAllFiles( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	int		nCurrentPos ;
	int		nCurrentLine ;
	int		nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , MARGIN_BOOKMARK_VALUE ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{

				for( nTabPageIndex++ ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);

					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , 0 , MARGIN_BOOKMARK_VALUE ) ;
					if( nFindLineNo != -1 )
					{
						SelectTabPageByIndex( nTabPageIndex );
						JumpGotoLine( p , nFindLineNo , 0 );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

void InitNavigateBackNextTraceList()
{
	INIT_LIST_HEAD( & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount = 0 ;

	return;
}

int AddNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos )
{
	if( nMaxNavigateBackNextTraceCount + 1 > MAX_TRACE_COUNT )
	{
		struct NavigateBackNextTrace *first = list_first_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;
		list_del( & (first->nodeNavigateBackNextTrace) );
		free( first );
		nMaxNavigateBackNextTraceCount--;
	}

	struct NavigateBackNextTrace *curr = (struct NavigateBackNextTrace*)malloc( sizeof(struct NavigateBackNextTrace) ) ;
	if( curr == NULL )
	{
		ConfirmErrorInfo( "申请内存失败用以存放导航退回结构" );
		return -2;
	}
	memset( curr , 0x00 , sizeof(struct NavigateBackNextTrace) );

	curr->pnodeTabPage = pnodeTabPage ;
	curr->hwndScintilla = pnodeTabPage->hwndScintilla ;
	curr->nLastPos = nCurrentPos ;

	list_add_tail( & (curr->nodeNavigateBackNextTrace) , & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount++;

	return 0;
}

int UpdateNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos )
{
	struct NavigateBackNextTrace *last = list_last_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;

	if( last && last->pnodeTabPage == pnodeTabPage )
		last->nLastPos = nCurrentPos ;
	
	return 0;
}

int OnSearchNavigateBackPrev_InThisFile()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->hwndScintilla != g_pnodeCurrentTabPage->hwndScintilla )
			return 1;
		
		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->hwndScintilla == g_pnodeCurrentTabPage->hwndScintilla )
			{
				int nTextLength = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
				int nGotoPos ;
				if( prev->nLastPos > nTextLength - 1 )
					nGotoPos = nTextLength - 1 ;
				else
					nGotoPos = prev->nLastPos ;
				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
			}
		}

		return 0;
	}

	return 1;
}

int OnSearchNavigateBackPrev_InAllFiles()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->pnodeTabPage != g_pnodeCurrentTabPage )
		{
			SelectTabPage( curr->pnodeTabPage , -1 );
			return 0;
		}

		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->pnodeTabPage != g_pnodeCurrentTabPage )
			{
				SelectTabPage( prev->pnodeTabPage , -1 );
			}

			int nTextLength = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
			int nGotoPos ;
			if( prev->nLastPos > nTextLength - 1 )
				nGotoPos = nTextLength - 1 ;
			else
				nGotoPos = prev->nLastPos ;
			g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
		}

		return 0;
	}

	return 1;
}

void CleanNavigateBackNextTraceListByThisFile( struct TabPage *pnodeTabPage )
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*next = NULL ;

	list_for_each_entry_safe( curr , next , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->hwndScintilla == pnodeTabPage->hwndScintilla )
		{
			list_del( & (curr->nodeNavigateBackNextTrace) );
			free( curr );
			nMaxNavigateBackNextTraceCount--;
		}
	}

	return;
}

