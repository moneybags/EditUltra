#include "framework.h"

struct list_head	listRemoteFileServer ;

INT_PTR CALLBACK RemoteFileServersWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND		hwndRemoteFileServersListBox = NULL ;
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	int			nRemoteFileServersListBoxItemIndex ;
	int			nRemoteFileServersListBoxItemCount ;
	static HWND		hwndCommProtocolComboBox ;
	char			numbuf[ 20 ] ;
	int			nret = 0 ;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		hwndRemoteFileServersListBox = ::GetDlgItem( hDlg , IDC_REMOTE_FILESERVERS_LISTBOX ) ;
		list_for_each_entry( pnodeRemoteFileServer , & listRemoteFileServer , struct RemoteFileServer , nodeRemoteFileServer )
		{
			nRemoteFileServersListBoxItemIndex = ListBox_AddString( hwndRemoteFileServersListBox , pnodeRemoteFileServer->acFileServerName );
			ListBox_SetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex , pnodeRemoteFileServer );
		}

		hwndCommProtocolComboBox = ::GetDlgItem( hDlg , IDC_COMMPROTOCOL_COMBOBOX ) ;
		ComboBox_AddString( hwndCommProtocolComboBox , "SFTP" );
		ComboBox_SetCurSel( hwndCommProtocolComboBox , 0 );

		SetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_PORT_EDIT) , "22" );

		CenterWindow( hDlg , g_hwndMainWindow );
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if( HIWORD(wParam) == LBN_SELCHANGE )
		{
			nRemoteFileServersListBoxItemIndex = ListBox_GetCurSel( hwndRemoteFileServersListBox ) ;
			if( nRemoteFileServersListBoxItemIndex >= 0 )
			{
				pnodeRemoteFileServer = (struct RemoteFileServer *)ListBox_GetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex ) ;

				SetWindowText( ::GetDlgItem(hDlg,IDC_FILESERVER_NAME_EDIT) , pnodeRemoteFileServer->acFileServerName );
				SetWindowText( ::GetDlgItem(hDlg,IDC_COMMPROTOCOL_COMBOBOX) , pnodeRemoteFileServer->acCommProtocol );
				SetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_ADDRESS_EDIT) , pnodeRemoteFileServer->acNetworkAddress );
				memset( numbuf , 0x00 ,sizeof(numbuf) );
				snprintf( numbuf , sizeof(numbuf)-1 , "%d" , pnodeRemoteFileServer->nNetworkPort );
				SetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_PORT_EDIT) , numbuf );
				SetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_USER_EDIT) , pnodeRemoteFileServer->acLoginUser );
				SetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_PASS_EDIT) , pnodeRemoteFileServer->acLoginPass );
			}
		}
		else if( LOWORD(wParam) == IDC_TEST_SERVER_BUTTON )
		{
			struct RemoteFileServer	nodeRemoteFileServer ;
			CURL			*curl = NULL ;
			CURLcode		res ;
			char			url[ 1024 ] ;
			char			userpwd[ 256 ] ;

			GetWindowText( ::GetDlgItem(hDlg,IDC_FILESERVER_NAME_EDIT) , nodeRemoteFileServer.acFileServerName , sizeof(nodeRemoteFileServer.acFileServerName) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_COMMPROTOCOL_COMBOBOX) , nodeRemoteFileServer.acCommProtocol , sizeof(nodeRemoteFileServer.acCommProtocol) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_ADDRESS_EDIT) , nodeRemoteFileServer.acNetworkAddress , sizeof(nodeRemoteFileServer.acNetworkAddress) );
			memset( numbuf , 0x00 ,sizeof(numbuf) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_PORT_EDIT) , numbuf , sizeof(numbuf) );
			nodeRemoteFileServer.nNetworkPort = atoi(numbuf) ;
			GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_USER_EDIT) , nodeRemoteFileServer.acLoginUser , sizeof(nodeRemoteFileServer.acLoginUser) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_PASS_EDIT) , nodeRemoteFileServer.acLoginPass , sizeof(nodeRemoteFileServer.acLoginPass) );

			if( nodeRemoteFileServer.acNetworkAddress[0] && nodeRemoteFileServer.nNetworkPort>0 && nodeRemoteFileServer.acLoginUser[0] )
			{
				if( nodeRemoteFileServer.acLoginPass[0] == '\0' )
				{
					nret = InputBox( g_hwndMainWindow , "请输入用户密码：" , "输入窗口" , 0 , nodeRemoteFileServer.acLoginPass , sizeof(nodeRemoteFileServer.acLoginPass)-1 ) ;
					if( nret == IDOK )
					{
						if( nodeRemoteFileServer.acLoginPass[0] == '\0' )
						{
							return 0;
						}
					}
					else if( nret == IDCANCEL )
					{
						return 0;
					}
					else
					{
						return 0;
					}
				}

				curl = curl_easy_init() ;
				if( curl == NULL )
				{
					::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
					return 0;
				}

				memset( url , 0x00 , sizeof(url) );
				snprintf( url , sizeof(url)-1 , "sftp://%s:%d/" , nodeRemoteFileServer.acNetworkAddress , nodeRemoteFileServer.nNetworkPort );
				curl_easy_setopt( curl , CURLOPT_URL , url );
				memset( userpwd , 0x00 , sizeof(userpwd) );
				snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , nodeRemoteFileServer.acLoginUser , nodeRemoteFileServer.acLoginPass );
				curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
				curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
				curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
				res = curl_easy_perform( curl ) ;
				if( res != CURLE_OK )
				{
					::MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
					curl_easy_cleanup( curl );
				}
				else
				{
					::MessageBox(NULL, TEXT("连接远程文件服务器成功"), TEXT("成功"), MB_ICONINFORMATION | MB_OK);
					curl_easy_cleanup( curl );
				}
			}
		}
		else if( LOWORD(wParam) == IDC_ADD_SERVER_BUTTON )
		{
			pnodeRemoteFileServer = (struct RemoteFileServer *)malloc( sizeof(struct RemoteFileServer) ) ;
			if( pnodeRemoteFileServer == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存用以存放远程文件服务器结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return 0;
			}
			memset( pnodeRemoteFileServer , 0x00 , sizeof(struct RemoteFileServer) );

			GetWindowText( ::GetDlgItem(hDlg,IDC_FILESERVER_NAME_EDIT) , pnodeRemoteFileServer->acFileServerName , sizeof(pnodeRemoteFileServer->acFileServerName) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_COMMPROTOCOL_COMBOBOX) , pnodeRemoteFileServer->acCommProtocol , sizeof(pnodeRemoteFileServer->acCommProtocol) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_ADDRESS_EDIT) , pnodeRemoteFileServer->acNetworkAddress , sizeof(pnodeRemoteFileServer->acNetworkAddress) );
			memset( numbuf , 0x00 ,sizeof(numbuf) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_PORT_EDIT) , numbuf , sizeof(numbuf) );
			pnodeRemoteFileServer->nNetworkPort = atoi(numbuf) ;
			GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_USER_EDIT) , pnodeRemoteFileServer->acLoginUser , sizeof(pnodeRemoteFileServer->acLoginUser) );
			GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_PASS_EDIT) , pnodeRemoteFileServer->acLoginPass , sizeof(pnodeRemoteFileServer->acLoginPass) );
			if( pnodeRemoteFileServer->acLoginPass[0] )
				pnodeRemoteFileServer->bConfigLoginPass = TRUE ;
			else
				pnodeRemoteFileServer->bConfigLoginPass = FALSE ;

			if( strchr( pnodeRemoteFileServer->acFileServerName , ' ' ) && strchr( pnodeRemoteFileServer->acFileServerName , '\t' ) )
			{
				::MessageBox(NULL, TEXT("连接名称中不能包含白字符"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return 0;
			}

			char			acPathFilename[ MAX_PATH ] ;
			struct TreeViewData	*tvd = NULL ;

			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/~/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
			tvd = AddFileTreeNode( hwndFileTree , TVI_ROOT , nFileTreeImageDrive , pnodeRemoteFileServer , acPathFilename , pnodeRemoteFileServer->acFileServerName , FALSE ) ;
			if( tvd == NULL )
			{
				::MessageBox(NULL, TEXT("不能增加目录文件树根节点"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return 0;
			}

			nRemoteFileServersListBoxItemIndex = ListBox_AddString( hwndRemoteFileServersListBox , pnodeRemoteFileServer->acFileServerName );
			ListBox_SetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex , pnodeRemoteFileServer );
			ListBox_SetCurSel( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex );
		}
		else if( LOWORD(wParam) == IDC_UPDATE_SERVER_BUTTON )
		{
			nRemoteFileServersListBoxItemIndex = ListBox_GetCurSel( hwndRemoteFileServersListBox ) ;
			if( nRemoteFileServersListBoxItemIndex >= 0 )
			{
				pnodeRemoteFileServer = (struct RemoteFileServer *)ListBox_GetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex ) ;

				GetWindowText( ::GetDlgItem(hDlg,IDC_FILESERVER_NAME_EDIT) , pnodeRemoteFileServer->acFileServerName , sizeof(pnodeRemoteFileServer->acFileServerName) );
				GetWindowText( ::GetDlgItem(hDlg,IDC_COMMPROTOCOL_COMBOBOX) , pnodeRemoteFileServer->acCommProtocol , sizeof(pnodeRemoteFileServer->acCommProtocol) );
				GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_ADDRESS_EDIT) , pnodeRemoteFileServer->acNetworkAddress , sizeof(pnodeRemoteFileServer->acNetworkAddress) );
				memset( numbuf , 0x00 ,sizeof(numbuf) );
				GetWindowText( ::GetDlgItem(hDlg,IDC_NETWORK_PORT_EDIT) , numbuf , sizeof(numbuf) );
				pnodeRemoteFileServer->nNetworkPort = atoi(numbuf) ;
				GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_USER_EDIT) , pnodeRemoteFileServer->acLoginUser , sizeof(pnodeRemoteFileServer->acLoginUser) );
				GetWindowText( ::GetDlgItem(hDlg,IDC_LOGIN_PASS_EDIT) , pnodeRemoteFileServer->acLoginPass , sizeof(pnodeRemoteFileServer->acLoginPass) );
				if( pnodeRemoteFileServer->acLoginPass[0] )
					pnodeRemoteFileServer->bConfigLoginPass = TRUE ;
				else
					pnodeRemoteFileServer->bConfigLoginPass = FALSE ;

				ListBox_DeleteString( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex );

				nRemoteFileServersListBoxItemIndex = ListBox_AddString( hwndRemoteFileServersListBox , pnodeRemoteFileServer->acFileServerName );
				ListBox_SetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex , pnodeRemoteFileServer );
				ListBox_SetCurSel( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex );
			}
		}
		else if( LOWORD(wParam) == IDC_REMOVE_SERVER_BUTTON )
		{
			nRemoteFileServersListBoxItemIndex = ListBox_GetCurSel( hwndRemoteFileServersListBox ) ;
			if( nRemoteFileServersListBoxItemIndex >= 0 )
			{
				pnodeRemoteFileServer = (struct RemoteFileServer *)ListBox_GetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex ) ;

				HTREEITEM		htiRoot ;
				struct TreeViewData	*tvd = NULL ;
				char			acConfigPathFilename[ MAX_PATH ] ;

				htiRoot = TreeView_GetRoot( hwndFileTree ) ;
				while( htiRoot )
				{
					tvd = GetTreeViewDataFromHTREEITEM( htiRoot ) ;
					if( tvd->pstRemoteFileServer )
					{
						if( pnodeRemoteFileServer == tvd->pstRemoteFileServer )
						{
							memset( acConfigPathFilename , 0x00 , sizeof(acConfigPathFilename) );
							snprintf( acConfigPathFilename , sizeof(acConfigPathFilename)-1 , "%s/conf/rfileser_%s.conf" , g_acModulePathName , tvd->pstRemoteFileServer->acFileServerName );
							::DeleteFile( acConfigPathFilename );

							free( tvd );
							TreeView_DeleteItem( hwndFileTree , htiRoot );

							break;
						}
					}

					htiRoot = TreeView_GetNextSibling( hwndFileTree , htiRoot ) ;
				}

				free( pnodeRemoteFileServer );

				ListBox_DeleteString( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex );

				nRemoteFileServersListBoxItemCount = ListBox_GetCount( hwndRemoteFileServersListBox ) ;
				if( nRemoteFileServersListBoxItemIndex == nRemoteFileServersListBoxItemCount )
					nRemoteFileServersListBoxItemIndex--;
				ListBox_SetCurSel( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex );
				::SendMessage( hDlg , WM_COMMAND , MAKELONG(0,LBN_SELCHANGE) , 0 );
			}
		}
		else if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			if( LOWORD(wParam) == IDOK )
			{
				INIT_LIST_HEAD( & listRemoteFileServer );

				nRemoteFileServersListBoxItemCount = ListBox_GetCount( hwndRemoteFileServersListBox ) ;
				for( nRemoteFileServersListBoxItemIndex = 0 ; nRemoteFileServersListBoxItemIndex < nRemoteFileServersListBoxItemCount ; nRemoteFileServersListBoxItemIndex++ )
				{
					pnodeRemoteFileServer = (struct RemoteFileServer *)ListBox_GetItemData( hwndRemoteFileServersListBox , nRemoteFileServersListBoxItemIndex ) ;

					list_add_tail( & (pnodeRemoteFileServer->nodeRemoteFileServer) , & listRemoteFileServer );
				}
			}

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

int LoadRemoteFileServersConfigFromFile( char *filebuf )
{
	char			acFindPathFilename[ MAX_PATH ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	char			*pcItemName = NULL ;
	char			*pcItemValue = NULL ;
	FILE			*fp = NULL ;
	unsigned char		acUnbase64UserPass[ sizeof(pnodeRemoteFileServer->acLoginPass) ] ;
	int			nret = 0 ;

	memset( acFindPathFilename , 0x00 , sizeof(acFindPathFilename) );
	sprintf( acFindPathFilename , "%s\\conf\\rfileser_*.conf" , g_acModulePathName );
	hFindFile = ::FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 1;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( ! ( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			pnodeRemoteFileServer = (struct RemoteFileServer *)malloc( sizeof(struct RemoteFileServer) ) ;
			if( pnodeRemoteFileServer == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存用以存放远程文件服务器结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return 0;
			}
			memset( pnodeRemoteFileServer , 0x00 , sizeof(struct RemoteFileServer) );

			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s\\conf\\%s" , g_acModulePathName , stFindFileData.cFileName );
			fp = fopen( acPathFilename , "r" ) ;
			if( fp == NULL )
				return -1;

			while(1)
			{
				memset( filebuf , 0x00 , FILEBUF_MAX );
				if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
					break;

				nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
				if( nret > 0 )
					continue;
				else if( nret < 0 )
				{
					fclose( fp );
					free( pnodeRemoteFileServer );
					return nret;
				}

				if( strcmp( pcItemName , "remote_fileserver.fileserver_name" ) == 0 )
				{
					strncpy( pnodeRemoteFileServer->acFileServerName , pcItemValue , sizeof(pnodeRemoteFileServer->acFileServerName)-1 );
				}
				else if( strcmp( pcItemName , "remote_fileserver.comm_protocol" ) == 0 )
				{
					strncpy( pnodeRemoteFileServer->acCommProtocol , pcItemValue , sizeof(pnodeRemoteFileServer->acCommProtocol)-1 );
				}
				else if( strcmp( pcItemName , "remote_fileserver.network_address" ) == 0 )
				{
					strncpy( pnodeRemoteFileServer->acNetworkAddress , pcItemValue , sizeof(pnodeRemoteFileServer->acNetworkAddress)-1 );
				}
				else if( strcmp( pcItemName , "remote_fileserver.network_port" ) == 0 )
				{
					pnodeRemoteFileServer->nNetworkPort = atoi( pcItemValue ) ;
				}
				else if( strcmp( pcItemName , "remote_fileserver.login_user" ) == 0 )
				{
					strncpy( pnodeRemoteFileServer->acLoginUser , pcItemValue , sizeof(pnodeRemoteFileServer->acLoginUser)-1 );
				}
				else if( strcmp( pcItemName , "remote_fileserver.login_pass" ) == 0 )
				{
					if( pcItemValue[0] )
					{
						memset( acUnbase64UserPass , 0x00 , sizeof(acUnbase64UserPass) );
						DEBUGHEXLOGC( pcItemValue , (int)strlen(pcItemValue) , "LoadRemoteFileServersConfigFromFile - pcItemValue , [%d]bytes" , (int)strlen(pcItemValue) )
							EVP_DecodeBlock( (unsigned char*)acUnbase64UserPass , (unsigned char*)pcItemValue , (int)strlen(pcItemValue) );
						DEBUGHEXLOGC( acUnbase64UserPass , (int)sizeof(acUnbase64UserPass)-1 , "LoadRemoteFileServersConfigFromFile - acUnbase64UserPass , [%d]bytes" , (int)sizeof(acUnbase64UserPass)-1 )
							memset( pnodeRemoteFileServer->acLoginPass , 0x00 , sizeof(pnodeRemoteFileServer->acLoginPass) );
						AesDecrypt( acUnbase64UserPass , (unsigned char *)(pnodeRemoteFileServer->acLoginPass) , sizeof(acUnbase64UserPass)-1 );
						DEBUGHEXLOGC( pnodeRemoteFileServer->acLoginPass , sizeof(pnodeRemoteFileServer->acLoginPass)-1 , "LoadRemoteFileServersConfigFromFile - pnodeRemoteFileServer->acLoginPass , [%d]bytes" , sizeof(pnodeRemoteFileServer->acLoginPass)-1 )
					}
					else
					{
						memset( pnodeRemoteFileServer->acLoginPass , 0x00 , sizeof(pnodeRemoteFileServer->acLoginPass) );
					}
				}
			}

			fclose( fp );

			if( pnodeRemoteFileServer->acFileServerName[0] == '\0' || pnodeRemoteFileServer->acCommProtocol[0] == '\0' || pnodeRemoteFileServer->acNetworkAddress[0] == '\0' || pnodeRemoteFileServer->nNetworkPort <= 0 || pnodeRemoteFileServer->acLoginUser[0] == '\0' )
			{
				::MessageBox(NULL, TEXT("远程文件服务器配置数据不合法"), TEXT("错误"), MB_ICONERROR | MB_OK);
				::DeleteFile( acPathFilename );
			}
			else
			{
				if( pnodeRemoteFileServer->acLoginPass[0] )
					pnodeRemoteFileServer->bConfigLoginPass = TRUE ;
				else
					pnodeRemoteFileServer->bConfigLoginPass = FALSE ;
				list_add_tail( & (pnodeRemoteFileServer->nodeRemoteFileServer) , & listRemoteFileServer );
			}
		}
	}
	while( ::FindNextFile( hFindFile , & stFindFileData ) );

	return 0;
}

int SaveRemoteFileServersConfigToFile()
{
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	char			acConfigPathFilename[ MAX_PATH ] ;
	FILE			*fp = NULL ;
	unsigned char		acEncriptUserPass[ sizeof(pnodeRemoteFileServer->acLoginPass) ] ;
	unsigned char		acBase64UserPass[ sizeof(pnodeRemoteFileServer->acLoginPass)*2 ] ;

	list_for_each_entry( pnodeRemoteFileServer , & listRemoteFileServer , struct RemoteFileServer , nodeRemoteFileServer )
	{
		memset( acConfigPathFilename , 0x00 , sizeof(acConfigPathFilename) );
		snprintf( acConfigPathFilename , sizeof(acConfigPathFilename)-1 , "%s\\conf\\rfileser_%s.conf" , g_acModulePathName , pnodeRemoteFileServer->acFileServerName );
		fp = fopen( acConfigPathFilename , "w" ) ;
		if( fp == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建远程文件服务器配置文件"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		if( pnodeRemoteFileServer->acLoginPass[0] )
		{
			DEBUGHEXLOGC( pnodeRemoteFileServer->acLoginPass , sizeof(pnodeRemoteFileServer->acLoginPass)-1 , "SaveRemoteFileServersConfigToFile - pnodeRemoteFileServer->acLoginPass , [%d]bytes" , sizeof(pnodeRemoteFileServer->acLoginPass)-1 )
			memset( acEncriptUserPass , 0x00 , sizeof(acEncriptUserPass) );
			AesEncrypt( (unsigned char *)(pnodeRemoteFileServer->acLoginPass) , acEncriptUserPass , sizeof(pnodeRemoteFileServer->acLoginPass)-1 );
			DEBUGHEXLOGC( acEncriptUserPass , sizeof(acEncriptUserPass)-1 , "SaveRemoteFileServersConfigToFile - acEncriptUserPass , [%d]bytes" , sizeof(acEncriptUserPass)-1 )
			memset( acBase64UserPass , 0x00 , sizeof(acBase64UserPass) );
			EVP_EncodeBlock( acBase64UserPass , acEncriptUserPass , sizeof(acEncriptUserPass)-1 );
			DEBUGHEXLOGC( acBase64UserPass , sizeof(acBase64UserPass)-1 , "SaveRemoteFileServersConfigToFile - acBase64UserPass , [%d]bytes" , sizeof(acBase64UserPass)-1 )
		}
		else
		{
			memset( acBase64UserPass , 0x00 , sizeof(acBase64UserPass) );
		}

		fprintf( fp , "remote_fileserver.fileserver_name = " ); ExpandConfigStringItemValue( fp , pnodeRemoteFileServer->acFileServerName );
		fprintf( fp , "remote_fileserver.comm_protocol = " ); ExpandConfigStringItemValue( fp , pnodeRemoteFileServer->acCommProtocol );
		fprintf( fp , "remote_fileserver.network_address = " ); ExpandConfigStringItemValue( fp , pnodeRemoteFileServer->acNetworkAddress );
		fprintf( fp , "remote_fileserver.network_port = %d\n" , pnodeRemoteFileServer->nNetworkPort );
		fprintf( fp , "remote_fileserver.login_user = " ); ExpandConfigStringItemValue( fp , pnodeRemoteFileServer->acLoginUser );
		fprintf( fp , "remote_fileserver.login_pass = " ); ExpandConfigStringItemValue( fp , (char*)acBase64UserPass );

		fclose( fp );
	}

	return 0;
}

int OnManageRemoteFileServers()
{
	int		nret = 0 ;

	nret = (int)DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_REMOTE_FILESERVERS_DIALOG), g_hwndMainWindow, RemoteFileServersWndProc) ;
	if( nret == IDOK )
	{
		SaveRemoteFileServersConfigToFile();
	}

	return 0;
}
