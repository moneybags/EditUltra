#include "framework.h"

HWND	hwndFileTreeBar = NULL ;
BOOL	bIsFileTreeBarShow = FALSE ;
RECT	rectFileTreeBar = { 0 } ;

HWND	hwndFileTree = NULL ;

HMENU	hFileTreePopupMenu ;

int	nFileTreeImageDrive ;
int	nFileTreeImageOpenFold ;
int	nFileTreeImageClosedFold ;
int	nFileTreeImageTextFile ;
int	nFileTreeImageGeneralFile ;
int	nFileTreeImageExecFile ;

static BOOL LoadTreeViewImageLists(HWND hwnd) 
{ 
	HIMAGELIST himl;  // handle to image list 
	HBITMAP hbmp;     // handle to bitmap 

	if ((himl = ImageList_Create(16, 16, ILC_COLOR, 6, 0)) == NULL) 
		return FALSE; 

	// Add the open file, closed file, and document bitmaps. 
	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_DRIVE)); 
	nFileTreeImageDrive = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_OPENFOLD)); 
	nFileTreeImageOpenFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_CLSDFOLD)); 
	nFileTreeImageClosedFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_TXT)); 
	nFileTreeImageGeneralFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_DOC)); 
	nFileTreeImageTextFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_EXE)); 
	nFileTreeImageExecFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	// Fail if not all of the images were added. 
	if (ImageList_GetImageCount(himl) < 6) 
		return FALSE; 

	// Associate the image list with the tree-view control. 
	TreeView_SetImageList(hwnd, himl, TVSIL_NORMAL); 

	return TRUE; 
}

int CreateFileTreeBar( HWND hWnd )
{
	RECT		rectMainClient ;
	TCITEM		tci ;

	BOOL		bret ;
	int		nret ;

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		::MessageBox(NULL, TEXT("不能注册文件树ReBar控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	GetClientRect( hWnd , & rectMainClient );
	memcpy( & rectFileTreeBar , & rectMainClient , sizeof(RECT) );
	rectFileTreeBar.left = rectMainClient.left ;
	rectFileTreeBar.right = rectFileTreeBar.left + FILETREEBAR_WIDTH_DEFAULT ;
	rectFileTreeBar.top = rectMainClient.top ;
	rectFileTreeBar.bottom = rectMainClient.bottom ;
	hwndFileTreeBar = ::CreateWindow( WC_TABCONTROL , NULL , WS_CHILD , rectFileTreeBar.left , rectFileTreeBar.top , rectFileTreeBar.right - rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top , hWnd , NULL , g_hAppInstance , NULL ) ;
	if( hwndFileTreeBar == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	SendMessage( hwndFileTreeBar , WM_SETFONT , (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);
	TabCtrl_SetPadding( hwndFileTreeBar , 20 , 5 );

	tci.mask = TCIF_TEXT ;
	tci.pszText = (char*)"文件资源管理器" ;
	nret = TabCtrl_InsertItem( hwndFileTreeBar , 0 , & tci ) ;
	if( nret == -1 )
	{
		::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	// hwndFileTree = ::CreateWindowEx( WS_EX_STATICEDGE , WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , rectFileTreeBar.left , rectFileTreeBar.top+TABS_HEIGHT_DEFAULT , rectFileTreeBar.right-rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top-TABS_HEIGHT_DEFAULT , hWnd , NULL , g_hAppInstance , NULL ) ; 
	hwndFileTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , rectFileTreeBar.left , rectFileTreeBar.top+TABS_HEIGHT_DEFAULT , rectFileTreeBar.right-rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top-TABS_HEIGHT_DEFAULT , hWnd , NULL , g_hAppInstance , NULL ) ; 
	if( hwndFileTree == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建文件树TreeView控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	LoadTreeViewImageLists( hwndFileTree );

	LoadDrivesToFileTree( hwndFileTree );

	LoadRemoteFilesToFileTree( hwndFileTree );

	::ShowWindow( hwndFileTreeBar , SW_HIDE);
	::UpdateWindow( hwndFileTreeBar );
	::ShowWindow( hwndFileTree , SW_HIDE);
	::UpdateWindow( hwndFileTree );

	hFileTreePopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_FILETREE_POPUPMENU) ) ;
	if( hFileTreePopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建目录文件树右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	hFileTreePopupMenu = GetSubMenu( hFileTreePopupMenu , 0 ) ;

	HMENU hMenu = GetMenu( g_hwndMainWindow ) ;
	CheckMenuItem(hMenu, IDM_VIEW_FILETREE, MF_UNCHECKED);

	return 0;
}

void AdjustFileTreeBox( RECT *rectFileTreeBar , RECT *rectFileTree )
{
	rectFileTree->left = FILETREE_MARGIN_LEFT ;
	rectFileTree->right = rectFileTreeBar->right - FILETREE_MARGIN_RIGHT ;
	rectFileTree->top = rectFileTreeBar->top + TABS_HEIGHT_DEFAULT + FILETREE_MARGIN_TOP ;
	rectFileTree->bottom = rectFileTreeBar->bottom - FILETREE_MARGIN_BOTTOM ;
	return;
}

static struct TreeViewData *AllocTreeViewData( struct RemoteFileServer *pstRemoteFileServer , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted , int nImageIndex )
{
	struct TreeViewData	*tvd = NULL ;

	tvd = (struct TreeViewData *)malloc( sizeof(struct TreeViewData) ) ;
	if( tvd == NULL )
		return NULL;
	memset( tvd , 0x00 , sizeof(struct TreeViewData) );

	tvd->pstRemoteFileServer = pstRemoteFileServer ;
	strcpy( tvd->acPathFilename , acPathFilename );
	strcpy( tvd->acFilename , acFilename );
	tvd->bIsLoadedCompleted = bIsLoadedCompleted ;
	tvd->nImageIndex = nImageIndex ;

	return tvd;
}

struct TreeViewData *GetTreeViewDataFromHTREEITEM( HTREEITEM hti )
{
	TVITEM		tviChild ;
	BOOL		bret ;

	memset( & tviChild , 0x00 , sizeof(TVITEM) );
	tviChild.mask = TVIF_HANDLE ;
	tviChild.hItem = hti ;
	bret = TreeView_GetItem( hwndFileTree , & tviChild ) ;
	if( bret != TRUE )
		return NULL;

	struct TreeViewData *tvd = (struct TreeViewData *)(tviChild.lParam) ;
	return tvd;
}

struct TreeViewData *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted )
{
	TVITEM			tvi ;
	TVINSERTSTRUCT		tvis;
	struct TreeViewData	*tvd = NULL ;

	tvd = AllocTreeViewData( pstRemoteFileServer , acPathFilename , acFilename , bIsLoadedCompleted , nImageIndex ) ;
	if( tvd == NULL )
		return NULL;

	tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_DI_SETITEM | TVIF_PARAM ;
	tvi.iImage = tvi.iSelectedImage = nImageIndex ;
	tvi.pszText = acFilename ;
	tvi.lParam = (LPARAM)tvd ;
	tvis.hParent = parent ;
	tvis.item = tvi;
	tvis.hInsertAfter = (parent==TVI_ROOT?TVI_LAST:TVI_SORT) ;
	tvd->hti = TreeView_InsertItem( hwnd , & tvis ) ;
	
	return tvd;
}

int AppendFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent )
{
	char			acFindPathFilename[ MAX_PATH+1 ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;

	sprintf( acFindPathFilename , "%s/*" , tvdParent->acPathFilename );
	hFindFile = ::FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , NULL , acPathFilename , stFindFileData.cFileName , FALSE ) ;
			if( tvd == NULL )
				return -1;
		}
		else
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , NULL , acPathFilename , stFindFileData.cFileName , TRUE ) ;
			if( tvd == NULL )
				return -1;
		}
	}
	while( ::FindNextFile( hFindFile , & stFindFileData ) );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadDrivesToFileTree( HWND hwnd )
{
	char			acDrivenames[ MAX_PATH ] ;
	char			*pcDrivename = NULL ;
	size_t			nDriveNameLength ;
	char			*p = NULL ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;

	::GetLogicalDriveStrings( sizeof(acDrivenames)-1 , acDrivenames ) ;
	pcDrivename = acDrivenames ;
	while( pcDrivename[0] )
	{
		nDriveNameLength = strlen(pcDrivename) ;

		p = strchr( pcDrivename , '\\' ) ;
		if( p )
			*(p) = '\0' ;
		tvd = AddFileTreeNode( hwnd , TVI_ROOT , nFileTreeImageDrive , NULL , pcDrivename , pcDrivename , TRUE ) ;
		if( tvd == NULL )
			return -1;

		nret = AppendFileTreeNodeChildren( hwnd , tvd ) ;
		if( nret )
			break;

		pcDrivename += nDriveNameLength + 1 ;
	}

	return 0;
}

static size_t ReadRemoteDirectory(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct RemoteFileBuffer	*rfb = (struct RemoteFileBuffer *)stream ;
	size_t			nTransferLen ;
	size_t			nReadLen ;

	nReadLen = nTransferLen = size * nmemb ;
	if( nReadLen > rfb->remain_len )
		nReadLen = rfb->remain_len ;

	memcpy( rfb->buf + rfb->str_len , buffer , nReadLen );
	rfb->str_len += nReadLen ;
	rfb->remain_len -= nReadLen ;

	return nTransferLen;
}

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent , CURL *curl )
{
	struct RemoteFileBuffer	*rfb = NULL ;
	char			userpwd[ 256 ] ;
	CURLcode		res ;
	char			*p1 = NULL ;
	char			*p2 = NULL ;
	int			count ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;

	if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
	{
		nret = InputBox( g_hwndMainWindow , "请输入用户密码：" , "输入窗口" , 0 , tvdParent->pstRemoteFileServer->acLoginPass , sizeof(tvdParent->pstRemoteFileServer->acLoginPass)-1 ) ;
		if( nret == IDOK )
		{
			if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
			{
				return 1;
			}
		}
		else if( nret == IDCANCEL )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	rfb = (struct RemoteFileBuffer *)malloc( sizeof(struct RemoteFileBuffer) ) ;
	if( rfb == NULL )
	{
		::MessageBox(NULL, TEXT("不能申请内存以存放远程文件列表缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return 1;
	}
	memset( rfb , 0x00 , sizeof(struct RemoteFileBuffer) );
	rfb->remain_len = REMOTE_FILE_BUFFER_SIZE_DEFAULT - 2 ;

	memset( userpwd , 0x00 , sizeof(userpwd) );
	snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdParent->pstRemoteFileServer->acLoginUser , tvdParent->pstRemoteFileServer->acLoginPass );
	curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
	curl_easy_setopt( curl , CURLOPT_WRITEDATA , rfb );
	res = curl_easy_perform( curl ) ;
	if( res != CURLE_OK )
	{
		::MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( rfb );
		if( tvdParent->pstRemoteFileServer->bConfigLoginPass == FALSE )
			memset( tvdParent->pstRemoteFileServer->acLoginPass , 0x00 , sizeof(tvdParent->pstRemoteFileServer->acLoginPass) );
		return -3;
	}

	DEBUGLOGC( "rfb->str_len[%d] ->buf[%.*s] " , rfb->str_len , rfb->str_len,rfb->buf )
	DEBUGHEXLOGC( rfb->buf , (long)(rfb->str_len) , "rfb->str_len[%d] ->remain_len[%d]" , rfb->str_len , rfb->remain_len )
	p1 = rfb->buf ;
	while( (*p1) )
	{
		p2 = strchr( p1+1 , '\n' ) ;
		if( p2 )
		{
			(*p2) = '\0' ;
			if( p2-1 > p1 && *(p2-1) == '\r' )
				*(p2-1) = '\0' ;
		}
		else
		{
			p2 = p1 + strlen(p1) ;
		}
		
		memset( acFilename , 0x00 , sizeof(acFilename) );
		count = sscanf( p1 , "%*s%*s%*s%*s%*s%*s%*s%*s%s" , acFilename ) ;
		if( count != 1 )
		{
			p1 = p2 + 1 ;
			continue;
		}

		if( strcmp(acFilename,".") == 0 || strcmp(acFilename,"..") == 0 )
		{
			p1 = p2 + 1 ;
			continue;
		}

		memset( acPathFilename , 0x00 , sizeof(acPathFilename) );

		if( p1[0] == 'd' )
		{
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s/" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , tvdParent->pstRemoteFileServer , acPathFilename , acFilename , FALSE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}
		else if( p1[0] == '-' )
		{
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , tvdParent->pstRemoteFileServer , acPathFilename , acFilename , TRUE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}

		p1 = p2 + 1 ;
	}

	free( rfb );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadRemoteFilesToFileTree( HWND hwnd )
{
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	char			acPathFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;

	list_for_each_entry( pnodeRemoteFileServer , & listRemoteFileServer , struct RemoteFileServer , nodeRemoteFileServer )
	{
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/~/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
		tvd = AddFileTreeNode( hwnd , TVI_ROOT , nFileTreeImageDrive , pnodeRemoteFileServer , acPathFilename , pnodeRemoteFileServer->acFileServerName , FALSE ) ;
		if( tvd == NULL )
			return -1;
	}

	return 0;
}

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent )
{
	struct TreeViewData	*tvdParent = NULL ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;
	BOOL			bret ;

	int			nret = 0 ;

	::SetCursor( LoadCursor(NULL,IDC_WAIT) );

	CURL			*curl = NULL ;
	char			url[ 1024 ] ;
	char			userpwd[ 256 ] ;

	tvdParent = GetTreeViewDataFromHTREEITEM( htiParent ) ;
	if( tvdParent->pstRemoteFileServer )
	{
		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
	}

	if( tvdParent->nImageIndex == nFileTreeImageDrive && tvdParent->pstRemoteFileServer && tvdParent->bIsLoadedCompleted == FALSE )
	{
		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );
		nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdParent , curl ) ;
		if( nret )
			return nret;
	}

	htiChild = TreeView_GetChild( hwnd , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( hwnd , & tviChild ) ;
		if( bret != TRUE )
		{
			::SetCursor( LoadCursor(NULL,IDC_ARROW) );
			if( tvdParent->pstRemoteFileServer )
				curl_easy_cleanup( curl );
			return -1;
		}

		struct TreeViewData *tvdChild = (struct TreeViewData *)(tviChild.lParam) ;
		if( tvdChild->bIsLoadedCompleted == FALSE )
		{
			if( tvdChild->pstRemoteFileServer == NULL )
			{
				nret = AppendFileTreeNodeChildren( hwnd , tvdChild ) ;
			}
			else
			{
				memset( url , 0x00 , sizeof(url) );
				strncpy( url , tvdChild->acPathFilename , sizeof(url)-1 );
				curl_easy_setopt( curl , CURLOPT_URL , url );
				memset( userpwd , 0x00 , sizeof(userpwd) );
				snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdChild->pstRemoteFileServer->acLoginUser , tvdChild->pstRemoteFileServer->acLoginPass );
				curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );

				nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdChild , curl ) ;
			}
			if( nret )
			{
				::SetCursor( LoadCursor(NULL,IDC_ARROW) );
				if( tvdChild->pstRemoteFileServer )
					curl_easy_cleanup( curl );
				return nret;
			}
		}

		htiChild = TreeView_GetNextSibling( hwnd , htiChild ) ;
	}

	if( tvdParent->pstRemoteFileServer )
		curl_easy_cleanup( curl );

	::SetCursor( LoadCursor(NULL,IDC_ARROW) );

	return 0;
}

int OnFileTreeNodeExpanding( NMTREEVIEW *lpnmtv )
{
	if( lpnmtv->action != 2 )
		return 0;

	UpdateFileTreeNodeChildren( hwndFileTree , lpnmtv->itemNew.hItem );

	return 0;
}

int OnFileTreeNodeDbClick( NMTREEVIEW *lpnmtv )
{
	HTREEITEM		hti ;
	struct DocTypeConfig	*pstDocTypeConfig ;
	char			acExtname[ MAX_PATH ] ;

	int			nret ;

	hti = TreeView_GetSelection( hwndFileTree ) ;
	if( hti == NULL )
		return 0;
	struct TreeViewData *tvd = GetTreeViewDataFromHTREEITEM( hti ) ;
	if( tvd->bIsLoadedCompleted == FALSE )
		return UpdateFileTreeNodeChildren( hwndFileTree , hti ) ;
	if( tvd->nImageIndex != nFileTreeImageTextFile )
		return 1;
	
	SplitPathFilename( tvd->acPathFilename , NULL , NULL , NULL , NULL , NULL , acExtname );
	pstDocTypeConfig = GetDocTypeConfig( acExtname ) ;
	if( pstDocTypeConfig == NULL )
	{
		int ID = MessageBox( NULL , "该文件不在文本编辑器支持文件类型中，是否继续打开？" , "文件类型警告" , MB_ICONWARNING|MB_YESNO  ) ;
		if( ID == IDNO )
			return 0;
	}

	if( tvd->pstRemoteFileServer == NULL )
		nret = OpenFileDirectly( tvd->acPathFilename ) ;
	else
		nret = OpenRemoteFileDirectly( tvd->pstRemoteFileServer , tvd->acPathFilename ) ;
	if( nret )
		return nret;

	return 0;
}

int RefreshFileTree()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvd = NULL ;
	struct TreeViewData	tvdSelect ;
	HTREEITEM		htiParent ;
	struct TreeViewData	*tvdParent = NULL ;
	HTREEITEM		htiFirstChild ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;

	BOOL			bret ;

	int			nret ;

	htiSelect = TreeView_GetSelection( hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvd = GetTreeViewDataFromHTREEITEM( htiSelect ) ;
	memcpy( & tvdSelect , tvd , sizeof(struct TreeViewData) );

	htiParent = TreeView_GetParent( hwndFileTree , htiSelect ) ;
	if( htiParent == NULL )
		return 0;
	tvdParent = GetTreeViewDataFromHTREEITEM( htiParent ) ;
	while(1)
	{
		htiFirstChild = TreeView_GetChild( hwndFileTree , htiParent ) ;
		if( htiFirstChild == NULL )
			break;
		tvd = GetTreeViewDataFromHTREEITEM( htiFirstChild ) ;
		free( tvd );

		TreeView_DeleteItem( hwndFileTree , htiFirstChild );
	}

	if( tvdParent->pstRemoteFileServer == NULL )
	{
		nret = AppendFileTreeNodeChildren( hwndFileTree , tvdParent ) ;
		if( nret )
			return nret;
	}
	else
	{
		CURL			*curl = NULL ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );

		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );

		nret = AppendRemoteFileTreeNodeChildren( hwndFileTree , tvdParent , curl ) ;
		if( nret )
		{
			curl_easy_cleanup( curl );
			return -1;
		}

		curl_easy_cleanup( curl );
	}

	nret = UpdateFileTreeNodeChildren( hwndFileTree , htiParent ) ;
	if( nret )
		return nret;

	htiChild = TreeView_GetChild( hwndFileTree , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( hwndFileTree , & tviChild ) ;
		if( bret != TRUE )
			return -1;

		struct TreeViewData *tvdChild = (struct TreeViewData *)(tviChild.lParam) ;
		if( strcmp(tvdChild->acPathFilename,tvdSelect.acPathFilename) == 0 && strcmp(tvdChild->acFilename,tvdSelect.acFilename) == 0 )
		{
			TreeView_SelectItem( hwndFileTree , htiChild );
			break;
		}

		htiChild = TreeView_GetNextSibling( hwndFileTree , htiChild ) ;
	}

	return 0;
}
