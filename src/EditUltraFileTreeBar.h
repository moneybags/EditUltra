#ifndef _H_EDITULTRA_FILETREEBAR_
#define _H_EDITULTRA_FILETREEBAR_

#include "framework.h"

extern HWND	hwndFileTreeBar ;
extern BOOL	bIsFileTreeBarShow ;
extern RECT	rectFileTreeBar ;

extern HWND	hwndFileTree ;

extern HMENU	hFileTreePopupMenu ;

extern int	nFileTreeImageDrive ;
extern int	nFileTreeImageOpenFold ;
extern int	nFileTreeImageClosedFold ;
extern int	nFileTreeImageTextFile ;
extern int	nFileTreeImageGeneralFile ;
extern int	nFileTreeImageExecFile ;

struct TreeViewData
{
	struct RemoteFileServer	*pstRemoteFileServer ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	HTREEITEM		hti ;
	int			nImageIndex ;
	BOOL			bIsLoadedCompleted ;
};

int CreateFileTreeBar( HWND hWnd );
void AdjustFileTreeBox( RECT *rectFileTreeBar , RECT *rectFileTree );

struct TreeViewData *GetTreeViewDataFromHTREEITEM( HTREEITEM hti );
struct TreeViewData *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted );

int AppendFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent );
int LoadDrivesToFileTree( HWND hwnd );

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent , CURL *curl );
int LoadRemoteFilesToFileTree( HWND hwnd );

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent );

int OnFileTreeNodeExpanding( NMTREEVIEW *lpnmtv );
int OnFileTreeNodeDbClick( NMTREEVIEW *lpnmtv );

int RefreshFileTree();

#define CONFIG_KEY_MATERIAL_FILETREE		"FILETR"

#define REMOTE_FILE_BUFFER_SIZE_DEFAULT		1024*1024

struct RemoteFileBuffer
{
	char	buf[ REMOTE_FILE_BUFFER_SIZE_DEFAULT ] ;
	size_t	str_len ;
	size_t	remain_len ;
};

#endif
