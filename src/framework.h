﻿// header.h: 标准系统包含文件的包含文件，
// 或特定于项目的包含文件
//

// #pragma once
#ifndef _H_FRAMEWORK_
#define _H_FRAMEWORK_

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN			 // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <richedit.h>
#include <Commdlg.h>
#include <commctrl.h>
#include <windowsx.h>
#include <shellapi.h>
// C 运行时头文件
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include "list.h"
#include "rbtree.h"

#include "LOGC.h"

#include "scintilla.h"
#include "SciLexer.h"

#include "pcre.h"

#include "curl/curl.h"

#include "openssl/sha.h"
#include "openssl/aes.h"
#include "openssl/evp.h"
#include "openssl/md5.h"
#include "openssl/des.h"

#include "iconv.h"

#include "mysql.h"

#include "oci.h"

#include "hiredis.h"

#include "InputBox.h"

#define OPEN_PATHFILENAME_RECENTLY_MAXCOUNT	20

#include "resource.h"
#include "EditUltraUtil.h"
#include "EditUltraEncoding.h"
#include "EditUltraRemoteFileServer.h"
#include "EditUltraStyleTheme.h"
#include "EditUltraSymbolListCtl.h"
#include "EditUltraSymbolTreeCtl.h"
#include "EditUltraQueryResultEditCtl.h"
#include "EditUltraQueryResultTableCtl.h"
#include "EditUltra.h"
#include "EditUltraConfig.h"
#include "EditUltraFileTreeBar.h"
#include "EditUltraTabPages.h"
#include "EditUltraEditCtl.h"
#include "EditUltraFile.h"
#include "EditUltraEdit.h"
#include "EditUltraSearch.h"
#include "EditUltraView.h"
#include "EditUltraEnv.h"
#include "EditUltraSourceCode.h"
#include "EditUltraDocType.h"

#include "minus.xpm"  
#include "plus.xpm"

#define TABS_HEIGHT_DEFAULT		21

#define MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK		2

#define FILETREE_MARGIN_LEFT		2
#define FILETREE_MARGIN_RIGHT		2
#define FILETREE_MARGIN_TOP		2
#define FILETREE_MARGIN_BOTTOM		2

#define SCINTILLA_MARGIN_LEFT		2
#define SCINTILLA_MARGIN_RIGHT		2
#define SCINTILLA_MARGIN_TOP		2
#define SCINTILLA_MARGIN_BOTTOM		2

#define MARGIN_LINENUMBER_INDEX		1
#define MARGIN_LINENUMBER_WIDTH		40

#define MARGIN_BOOKMARK_INDEX		2
#define MARGIN_BOOKMARK_WIDTH		16
#define MARGIN_BOOKMARK_MASKN		0x02
#define MARGIN_BOOKMARK_VALUE		4

#define MARGIN_FOLD_INDEX		3
#define MARGIN_FOLD_WIDTH		16

#define SYMBOLLIST_MARGIN_LEFT		2
#define SYMBOLLIST_MARGIN_RIGHT		2
#define SYMBOLLIST_MARGIN_TOP		2
#define SYMBOLLIST_MARGIN_BOTTOM	2

#define SYMBOLTREE_MARGIN_LEFT		2
#define SYMBOLTREE_MARGIN_RIGHT		2
#define SYMBOLTREE_MARGIN_TOP		2
#define SYMBOLTREE_MARGIN_BOTTOM	2

#define FILETREEBAR_WIDTH_DEFAULT	250
#define FILETREEBAR_WIDTH_MIN		100

#define SYMBOLLIST_WIDTH_DEFAULT	300
#define SYMBOLLIST_WIDTH_MIN		100

#define SYMBOLTREE_WIDTH_DEFAULT		300
#define TREEVIEW_WIDTH_MIN		100

#define SQLQUERYRESULT_EDIT_HEIGHT_DEFAULT	80
#define SQLQUERYRESULT_EDIT_HEIGHT_MIN		40

#define SQLQUERYRESULT_LISTVIEW_HEIGHT_DEFAULT	260
#define SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN	40

#define SPLIT_WIDTH			6

#define PATTERN_OVECCOUNT		30

#define CHAR_WIDTH			8

#define WHITESPACE_SIZE			3

#define EDITOR_TAB_WIDTH_DEFAULT	8

#define FONTSIZE_ZOOMOUT_ADJUSTVALUE	3

#define MAX_TRACE_COUNT			100

extern struct EditUltraMainConfig	g_stEditUltraMainConfig ;
extern struct StyleTheme		g_stStyleTheme ;
extern struct DocTypeConfig		g_astDocTypeConfig[] ;
extern struct list_head			listRemoteFileServer ;

#endif
