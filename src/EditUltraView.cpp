#include "framework.h"

int OnViewFileTree( struct TabPage *pnodeTabPage )
{
	if( bIsFileTreeBarShow == FALSE )
	{
		bIsFileTreeBarShow = TRUE ;

		rectFileTreeBar.right = g_stEditUltraMainConfig.nFileTreeBarWidth ;
		g_rectTabPages.left = g_stEditUltraMainConfig.nFileTreeBarWidth + SPLIT_WIDTH ;
	}
	else
	{
		bIsFileTreeBarShow = FALSE ;

		rectFileTreeBar.right = 0 ;
		g_rectTabPages.left = 0 ;
	}

	UpdateAllWindows();

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnViewStyleTheme( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	nret = (int)DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_STYLETHEME_DIALOG), g_hwndMainWindow, StyleThemeWndProc) ;
	if( nret == IDOK )
	{
		int		nTabPagesCount ;
		int		nTabPageIndex ;
		TCITEM		tci ;
		struct TabPage	*p = NULL ;

		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);

			InitTabPageControlsCommonStyle( p );
			/*
			p->pfuncScintilla( p->pScintilla , SCI_STYLESETFONT , STYLE_DEFAULT , (sptr_t)(stStyleTheme.text.font) );
			p->pfuncScintilla( p->pScintilla , SCI_STYLESETSIZE , STYLE_DEFAULT , stStyleTheme.text.fontsize );
			p->pfuncScintilla( p->pScintilla , SCI_STYLECLEARALL , 0 , 0 );
			*/

			InitTabPageControlsAfterLoadFile( p );
			
			/*
			if( p->pstDocTypeConfig->pfuncInitTabPageControlsAfterLoadFile )
				p->pstDocTypeConfig->pfuncInitTabPageControlsAfterLoadFile( p );
			*/

			if( p->hwndSymbolList )
			{
				HFONT hFont = CreateFont(-g_stStyleTheme.text.fontsize, 0, 0, 0, (g_stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleTheme.text.font); 
				::SendMessage( p->hwndSymbolList , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndSymbolList , NULL , TRUE );
			}

			if( pnodeTabPage->hwndSymbolTree )
			{
				HFONT hFont = CreateFont(-g_stStyleTheme.text.fontsize, 0, 0, 0, (g_stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleTheme.text.font); 
				::SendMessage( p->hwndSymbolTree , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndSymbolTree , NULL , TRUE );
			}

			if( pnodeTabPage->hwndQueryResultEdit )
			{
				HFONT hFont = CreateFont(-g_stStyleTheme.text.fontsize, 0, 0, 0, (g_stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleTheme.text.font); 
				::SendMessage( p->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndQueryResultEdit , NULL , TRUE );
			}

			if( pnodeTabPage->hwndQueryResultTable )
			{
				HFONT hFont = CreateFont(-g_stStyleTheme.text.fontsize, 0, 0, 0, (g_stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleTheme.text.font); 
				::SendMessage( p->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndQueryResultTable , NULL , TRUE );
			}
		}

		SaveStyleThemeConfigFile();
	}

	return 0;
}

int OnViewTabWidth( struct TabPage *pnodeTabPage )
{
	char		acTabWidth[ 20 ] ;

	int		nret = 0 ;

	memset( acTabWidth , 0x00 , sizeof(acTabWidth) );
	snprintf( acTabWidth , sizeof(acTabWidth)-1 , "%d" , g_stEditUltraMainConfig.nTabWidth );
	nret = InputBox( g_hwndMainWindow , "请输入制表符宽度：" , "输入窗口" , 0 , acTabWidth , sizeof(acTabWidth)-1 ) ;
	if( nret == IDOK )
	{
		if( acTabWidth[0] )
		{
			int		nTabPagesCount ;
			int		nTabPageIndex ;
			TCITEM		tci ;
			struct TabPage	*p = NULL ;

			g_stEditUltraMainConfig.nTabWidth = atoi(acTabWidth) ;

			UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

			SaveMainConfigFile();

			nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
			for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
			{
				memset( & tci , 0x00 , sizeof(TCITEM) );
				tci.mask = TCIF_PARAM ;
				TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
				p = (struct TabPage *)(tci.lParam);
				p->pfuncScintilla( p->pScintilla , SCI_SETTABWIDTH , g_stEditUltraMainConfig.nTabWidth , 0 );
			}
		}
	}

	return 0;
}

int OnViewWrapLineMode( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETWRAPMODE , 0 , 0 ) == 0 )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETWRAPMODE , 2 , 0 );
	else
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETWRAPMODE , 0 , 0 );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnViewLineNumberVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bLineNumberVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bLineNumberVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bLineNumberVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_LINENUMBER_INDEX , (g_stEditUltraMainConfig.bLineNumberVisiable==TRUE?MARGIN_LINENUMBER_WIDTH:0) );
	}

	return 0;
}

int OnViewBookmarkVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bBookmarkVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bBookmarkVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bBookmarkVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_BOOKMARK_INDEX , (g_stEditUltraMainConfig.bBookmarkVisiable==TRUE?MARGIN_BOOKMARK_WIDTH:0) );
	}

	return 0;
}

int OnViewWhiteSpaceVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bWhiteSpaceVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bWhiteSpaceVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bWhiteSpaceVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWWS , (g_stEditUltraMainConfig.bWhiteSpaceVisiable==TRUE?SCWS_VISIBLEALWAYS:SCWS_INVISIBLE) , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETWHITESPACESIZE , g_stEditUltraMainConfig.nWhiteSpaceSize , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETTABDRAWMODE , SCTD_LONGARROW , 0 );
	}

	return 0;
}

int OnViewNewLineVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bNewLineVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bNewLineVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bNewLineVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWEOL , g_stEditUltraMainConfig.bNewLineVisiable , 0 );
	}

	return 0;
}

int OnViewIndentationGuidesVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bIndentationGuidesVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bIndentationGuidesVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bIndentationGuidesVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETINDENTATIONGUIDES , (g_stEditUltraMainConfig.bIndentationGuidesVisiable?SC_IV_LOOKBOTH:SC_IV_NONE) , 0 );
	}

	return 0;
}

int OnViewZoomOut( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ZOOMOUT , 0 , 0 );

	return 0;
}

int OnViewZoomIn( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ZOOMIN , 0 , 0 );

	return 0;
}

int OnViewZoomReset( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETZOOM , g_nZoomReset , 0 );

	return 0;
}
