#include "framework.h"

int CreateScintillaControl( struct TabPage *pnodeTabPage )
{
	AdjustTabPageBox( pnodeTabPage );

	// pnodeTabPage->hwndScintilla = ::CreateWindow( "Scintilla" , "Scintilla" , WS_BORDER|WS_CHILD|WS_VSCROLL|WS_HSCROLL|WS_CLIPCHILDREN , pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.top , pnodeTabPage->rectScintilla.right-pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.bottom-pnodeTabPage->rectScintilla.top , g_hwndMainClient , 0 , g_hAppInstance , 0) ;
	pnodeTabPage->hwndScintilla = ::CreateWindow( "Scintilla" , "Scintilla" , WS_CHILD|WS_VSCROLL|WS_HSCROLL|WS_CLIPCHILDREN , pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.top , pnodeTabPage->rectScintilla.right-pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.bottom-pnodeTabPage->rectScintilla.top , g_hwndMainWindow , 0 , g_hAppInstance , 0) ;
	if( pnodeTabPage->hwndScintilla == NULL )
	{
		::MessageBox(g_hwndMainWindow, TEXT("不能创建Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	::ShowWindow( pnodeTabPage->hwndScintilla , SW_SHOW);
	::UpdateWindow( pnodeTabPage->hwndScintilla );

	pnodeTabPage->pfuncScintilla = (SciFnDirect)SendMessage( pnodeTabPage->hwndScintilla, SCI_GETDIRECTFUNCTION, 0, 0);
	pnodeTabPage->pScintilla = (sptr_t)SendMessage( pnodeTabPage->hwndScintilla, SCI_GETDIRECTPOINTER, 0, 0);
	
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_USEPOPUP , 0 , 0 );

	g_nZoomReset = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETZOOM , 0 , 0 );

	return 0;
}

void DestroyScintillaControl( struct TabPage *pnodeTabPage )
{
	DestroyWindow( pnodeTabPage->hwndScintilla );

	return;
}

int InitTabPageControlsCommonStyle( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , STYLE_DEFAULT , (sptr_t)(g_stStyleTheme.text.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , STYLE_DEFAULT , g_stStyleTheme.text.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , STYLE_DEFAULT , g_stStyleTheme.text.color );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , STYLE_DEFAULT , g_stStyleTheme.text.bold );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLECLEARALL , 0 , 0 );

	return 0;
}

int InitTabPageControlsBeforeLoadFile( struct TabPage *pnodeTabPage )
{
	InitTabPageControlsCommonStyle( pnodeTabPage );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CANCEL , 0 , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER , 0 , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETUNDOCOLLECTION , 0 , 0 );

	if( pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncInitTabPageControlsBeforeLoadFile )
	{
		pnodeTabPage->pstDocTypeConfig->pfuncInitTabPageControlsBeforeLoadFile( pnodeTabPage ) ;
	}

	return 0;
}

int InitTabPageControlsAfterLoadFile( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCARETLINEVISIBLE , TRUE , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCARETLINEVISIBLEALWAYS , 1 , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCARETLINEBACK , g_stStyleTheme.caretline.color , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINTYPEN , MARGIN_LINENUMBER_INDEX , SC_MARGIN_NUMBER );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_LINENUMBER_INDEX , (g_stEditUltraMainConfig.bLineNumberVisiable==TRUE?MARGIN_LINENUMBER_WIDTH:0) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINTYPEN, MARGIN_BOOKMARK_INDEX , SC_MARGIN_SYMBOL );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINMASKN, MARGIN_BOOKMARK_INDEX , MARGIN_BOOKMARK_MASKN ); 
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINWIDTHN, MARGIN_BOOKMARK_INDEX , (g_stEditUltraMainConfig.bBookmarkVisiable==TRUE?MARGIN_BOOKMARK_WIDTH:0) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINSENSITIVEN, MARGIN_BOOKMARK_INDEX , TRUE );
	// pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE, MARGIN_BOOKMARK_MASKN , SC_MARK_BOOKMARK );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETTABWIDTH , g_stEditUltraMainConfig.nTabWidth , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETWRAPMODE , 2 , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETHSCROLLBAR , 1 , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETVIEWWS , (g_stEditUltraMainConfig.bWhiteSpaceVisiable==TRUE?SCWS_VISIBLEALWAYS:SCWS_INVISIBLE) , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETWHITESPACESIZE , g_stEditUltraMainConfig.nWhiteSpaceSize , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETTABDRAWMODE , SCTD_LONGARROW , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETVIEWEOL , g_stEditUltraMainConfig.bNewLineVisiable , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETINDENTATIONGUIDES , (g_stEditUltraMainConfig.bIndentationGuidesVisiable?SC_IV_LOOKBOTH:SC_IV_NONE) , 0 );

	// pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMOUSEDOWNCAPTURES , true , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETUNDOCOLLECTION, 1, 0);
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

	int nEolMode = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETEOLMODE , 0 , 0 ) ;
	if( nEolMode == 0 )
		strcpy( pnodeTabPage->acEndOfLine , "\r\n" );
	else if( nEolMode == 1 )
		strcpy( pnodeTabPage->acEndOfLine , "\r" );
	else if( nEolMode == 2 )
		strcpy( pnodeTabPage->acEndOfLine , "\n" );
	else
		strcpy( pnodeTabPage->acEndOfLine , "\r\n" );

	if( pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncInitTabPageControlsAfterLoadFile )
	{
		pnodeTabPage->pstDocTypeConfig->pfuncInitTabPageControlsAfterLoadFile( pnodeTabPage ) ;
	}

	return 0;
}

int CleanTabPageControls( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncCleanTabPageControls )
	{
		pnodeTabPage->pstDocTypeConfig->pfuncCleanTabPageControls( pnodeTabPage ) ;
	}

	return 0;
}

void GetTextByRange( struct TabPage *pnodeTabPage , int start , int end , char *text )
{
	TEXTRANGE tr;
	tr.chrg.cpMin = start;
	tr.chrg.cpMax = end;
	tr.lpstrText = text;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, EM_GETTEXTRANGE, 0, reinterpret_cast<LPARAM>(&tr));
}

void GetTextByLine( struct TabPage *pnodeTabPage , int nLineNo , char *acText , int nTextBufsize )
{
	TEXTRANGE tr;
	tr.chrg.cpMin = (LONG)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_POSITIONFROMLINE , nLineNo , 0 );
	tr.chrg.cpMax = (LONG)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINEENDPOSITION , nLineNo , 0 );
	if( tr.chrg.cpMax - tr.chrg.cpMin + 1 > nTextBufsize - 1 )
	{
		tr.chrg.cpMax = tr.chrg.cpMin + ( nTextBufsize - 1 ) ;
	}
	tr.lpstrText = acText;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, EM_GETTEXTRANGE, 0, reinterpret_cast<LPARAM>(&tr));
}

bool IsDocumentModified( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage && pnodeTabPage->pfuncScintilla )
		return pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETMODIFY, 0, 0 );
	else
		return true;
}

int QueryIndexFromTabPage( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
			return nTabPageIndex;
	}

	return -1;
}

int OnSavePointReached( struct TabPage *pnodeTabPage )
{
	int		nTabPageIndex ;

	if( pnodeTabPage == NULL )
		return -1;

	nTabPageIndex = QueryIndexFromTabPage( pnodeTabPage ) ;
	if( nTabPageIndex < 0 )
		return -2;

	if( pnodeTabPage->acFilename[pnodeTabPage->nFilenameLen] == '*' )
	{
		pnodeTabPage->acFilename[pnodeTabPage->nFilenameLen] = '\0' ;
	}
	SetTabPageTitle( nTabPageIndex , pnodeTabPage->acFilename );
	SetWindowTitle( pnodeTabPage->acFilename );
	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnSavePointLeft( struct TabPage *pnodeTabPage )
{
	int		nTabPageIndex ;

	if( pnodeTabPage == NULL )
		return -1;

	nTabPageIndex = QueryIndexFromTabPage( pnodeTabPage ) ;
	if( nTabPageIndex < 0 )
		return -2;

	if( pnodeTabPage->acFilename[pnodeTabPage->nFilenameLen] != '*' )
	{
		pnodeTabPage->acFilename[pnodeTabPage->nFilenameLen] = '*' ;
		pnodeTabPage->acFilename[pnodeTabPage->nFilenameLen+1] = '\0' ;
	}
	SetTabPageTitle( nTabPageIndex , pnodeTabPage->acFilename );
	SetWindowTitle( pnodeTabPage->acFilename );
	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnMarginClick( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int nLineNumber = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_LINEFROMPOSITION , lpnotify->position , 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_TOGGLEFOLD , nLineNumber , 0 );

	return 0;
}

int OnCharAdded( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	if( pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncOnCharAdded )
		return pnodeTabPage->pstDocTypeConfig->pfuncOnCharAdded( pnodeTabPage , lpnotify );
	else
		return 0;
}
