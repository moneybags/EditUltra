#ifndef _H_EDITULTRA_CONFIG_
#define _H_EDITULTRA_CONFIG_

#include "framework.h"

#define FILEBUF_MAX	1024*1024

struct EditUltraMainConfig
{
	BOOL		bSetReadOnlyAfterOpenFile ;
	char		aacOpenPathFilenameRecently[OPEN_PATHFILENAME_RECENTLY_MAXCOUNT][MAX_PATH] ;
	BOOL		bUpdateWhereSelectTabPage ;
	int		nNewFileEols ;
	UINT		nNewFileEncoding ;
	BOOL		bEnableAutoAddCloseChar ;
	BOOL		bEnableAutoIdentation ;

	BOOL		bLineNumberVisiable ;
	BOOL		bBookmarkVisiable ;
	BOOL		bWhiteSpaceVisiable ;
	int		nWhiteSpaceSize ;
	BOOL		bNewLineVisiable ;
	BOOL		bIndentationGuidesVisiable ;

	BOOL		bBlockFoldVisiable ;

	char		acProcessFileCommand[ 1024 ] ;
	char		acProcessTextCommand[ 1024 ] ;

	int		nTabWidth ;

	int		nFileTreeBarWidth ;
	int		nSymbolListWidth ;
	int		nSymbolTreeWidth ;
	int		nSqlQueryResultEditHeight ;
	int		nSqlQueryResultListViewHeight ;

	BOOL		bEnableAutoCompletedShow ;
	int		nAutoCompletedShowAfterInputCharacters ;
	BOOL		bEnableCallTipShow ;
} ;

void SetEditUltraMainConfigDefault( struct EditUltraMainConfig *pstEditUltraConfig );

int LoadMainConfigFile( char *filebuf );
void FoldConfigStringItemValue( char *value );
int DecomposeConfigFileBuffer( char *filebuf , char **ppcItemName , char **ppcItemValue );
int LoadConfig();
void ExpandConfigStringItemValue( FILE *fp , char *value );
int SaveMainConfigFile();

#endif
