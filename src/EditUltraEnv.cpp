#include "framework.h"

BOOL g_bIsEnvFilePopupMenuSelected ;
BOOL g_bIsEnvDirectoryPopupMenuSelected ;

#define OPEN_FILE_WITH_EDITULTRA	"用 EditUltra 打开文件"

int OnEnvFilePopupMenu()
{
	HKEY		regkey_EditUltra ;
	HKEY		regkey_EditUltra_command ;
	LSTATUS		lsret ;

	if( g_bIsEnvFilePopupMenuSelected == FALSE )
	{
		char		acCommand[ MAX_PATH ] ;

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra" , & regkey_EditUltra ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表创建项\"*\\shell\\EditUltra\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表创建项\"*\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( regkey_EditUltra , NULL , REG_SZ , OPEN_FILE_WITH_EDITULTRA , (DWORD)sizeof(OPEN_FILE_WITH_EDITULTRA) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\EditUltra:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra );
			return -1;
		}

		lsret = RegSetKeyValue( regkey_EditUltra , NULL , "Icon" , REG_SZ , g_acModuleFileName , (DWORD)strlen(g_acModuleFileName)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\EditUltra:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra );
			return -1;
		}

		RegCloseKey( regkey_EditUltra );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表创建项\"*\\shell\\EditUltra\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\"" , g_acModuleFileName );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\EditUltra:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		::MessageBox(NULL, TEXT("在资源管理器注册右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		SetMenuItemChecked( g_hwndMainWindow, IDM_ENV_FILE_POPUPMENU, true);

		g_bIsEnvFilePopupMenuSelected = TRUE ;
	}
	else
	{
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表删除项\"*\\shell\\EditUltra\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表删除项\"*\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表删除项\"*\\shell\\EditUltra\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表删除项\"*\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		if( lsret == ERROR_SUCCESS )
			::MessageBox(NULL, TEXT("在资源管理器卸载右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		SetMenuItemChecked( g_hwndMainWindow, IDM_ENV_FILE_POPUPMENU, false);

		g_bIsEnvFilePopupMenuSelected = FALSE ;
	}

	return 0;
}

#define OPEN_DIRECTORYFILES_WITH_EDITULTRA	"用 EditUltra 打开目录中所有文件"

int OnEnvDirectoryPopupMenu()
{
	HKEY		regkey_EditUltra ;
	HKEY		regkey_EditUltra_command ;
	LSTATUS		lsret ;

	if( g_bIsEnvDirectoryPopupMenuSelected == FALSE )
	{
		char		acCommand[ MAX_PATH ] ;

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra" , & regkey_EditUltra ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表创建项\"Directory\\shell\\EditUltra\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( regkey_EditUltra , NULL , REG_SZ , OPEN_DIRECTORYFILES_WITH_EDITULTRA , (DWORD)sizeof(OPEN_DIRECTORYFILES_WITH_EDITULTRA) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EditUltra:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra );
			return -1;
		}

		lsret = RegSetKeyValue( regkey_EditUltra , NULL , "Icon" , REG_SZ , g_acModuleFileName , (DWORD)strlen(g_acModuleFileName)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EditUltra:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra );
			return -1;
		}

		RegCloseKey( regkey_EditUltra );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EditUltra\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\"" , g_acModuleFileName );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			::MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EditUltra:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		::MessageBox(NULL, TEXT("在资源管理器注册右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		SetMenuItemChecked( g_hwndMainWindow, IDM_ENV_DIRECTORY_POPUPMENU, true);

		g_bIsEnvDirectoryPopupMenuSelected = TRUE ;
	}
	else
	{
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EditUltra\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				::MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EditUltra\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				::MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EditUltra\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		if( lsret == ERROR_SUCCESS )
			::MessageBox(NULL, TEXT("在资源管理器卸载右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		SetMenuItemChecked( g_hwndMainWindow, IDM_ENV_DIRECTORY_POPUPMENU, false);

		g_bIsEnvDirectoryPopupMenuSelected = FALSE ;
	}

	return 0;
}

int OnEnvSetProcessFileCommand()
{
	char	acProcessFileCommand[ sizeof(g_stEditUltraMainConfig.acProcessFileCommand) ] ;
	int	nret = 0 ;

	memset( acProcessFileCommand , 0x00 , sizeof(acProcessFileCommand) );
	strcpy( acProcessFileCommand , g_stEditUltraMainConfig.acProcessFileCommand );
	nret = InputBox( g_hwndMainWindow , "请输入处理文件命令行\n（可以用\"%F\"占位文件名）：" , "输入窗口" , 0 , acProcessFileCommand , sizeof(acProcessFileCommand)-1 ) ;
	if( nret == IDOK )
	{
		if( acProcessFileCommand[0] == '\0' )
		{
			return 0;
		}
	}
	else if( nret == IDCANCEL )
	{
		return 0;
	}
	else
	{
		return 0;
	}

	strcpy( g_stEditUltraMainConfig.acProcessFileCommand , acProcessFileCommand );

	SaveMainConfigFile();

	return 0;
}

int OnEnvExecuteProcessFileCommand( struct TabPage *pnodeTabPage )
{
	char	*p = NULL ;
	char	cmd[ sizeof(g_stEditUltraMainConfig.acProcessFileCommand) * 2 ] ;
	BOOL	bret ;

	if( pnodeTabPage == NULL )
		return 0;
	if( g_stEditUltraMainConfig.acProcessFileCommand[0] == '\0' )
		return 0;

	memset( cmd , 0x00 , sizeof(cmd) );
	p = strstr( g_stEditUltraMainConfig.acProcessFileCommand , "%F" ) ;
	if( p )
	{
		snprintf( cmd , sizeof(cmd)-1 , "%.*s%s%s" , (int)(p-g_stEditUltraMainConfig.acProcessFileCommand),g_stEditUltraMainConfig.acProcessFileCommand , pnodeTabPage->acPathFilename , p+2 );
	}
	else
	{
		strcpy( cmd , g_stEditUltraMainConfig.acProcessFileCommand );
	}

	STARTUPINFO si ;
	PROCESS_INFORMATION pi ;
	memset( & si , 0x00 , sizeof(STARTUPINFO) );
	si.cb = sizeof(STARTUPINFO) ;
	memset( & pi , 0x00 , sizeof(PROCESS_INFORMATION) );
	bret = CreateProcess( NULL , cmd , NULL , NULL , NULL , 0 , NULL , NULL , & si , & pi ) ;
	if( bret == TRUE )
		CloseHandle( pi.hProcess );

	return 0;
}

int OnEnvSetProcessTextCommand()
{
	char	acProcessTextCommand[ sizeof(g_stEditUltraMainConfig.acProcessTextCommand) ] ;
	int	nret = 0 ;

	memset( acProcessTextCommand , 0x00 , sizeof(acProcessTextCommand) );
	strcpy( acProcessTextCommand , g_stEditUltraMainConfig.acProcessTextCommand );
	nret = InputBox( g_hwndMainWindow , "请输入处理文件命令行\n（可以用\"%T\"占位文本）：" , "输入窗口" , 0 , acProcessTextCommand , sizeof(acProcessTextCommand)-1 ) ;
	if( nret == IDOK )
	{
		if( acProcessTextCommand[0] == '\0' )
		{
			return 0;
		}
	}
	else if( nret == IDCANCEL )
	{
		return 0;
	}
	else
	{
		return 0;
	}

	strcpy( g_stEditUltraMainConfig.acProcessTextCommand , acProcessTextCommand );

	SaveMainConfigFile();

	return 0;
}

int OnEnvExecuteProcessTextCommand( struct TabPage *pnodeTabPage )
{
	int	nSelStartPos ;
	int	nSelEndPos ;
	int	nSelTextLength ;
	char	*acSelText = NULL ;

	char	*p = NULL ;
	char	cmd[ sizeof(g_stEditUltraMainConfig.acProcessFileCommand) * 2 ] ;

	BOOL	bret ;

	if( pnodeTabPage == NULL )
		return 0;
	if( g_stEditUltraMainConfig.acProcessTextCommand[0] == '\0' )
		return 0;

	nSelStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	nSelEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	nSelTextLength = nSelEndPos - nSelStartPos ;
	acSelText = (char*)malloc( nSelTextLength+1 ) ;
	if( acSelText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放SQL"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acSelText , 0x00 , nSelTextLength+1 );
	GetTextByRange( pnodeTabPage , nSelStartPos , nSelEndPos , acSelText );

	memset( cmd , 0x00 , sizeof(cmd) );
	p = strstr( g_stEditUltraMainConfig.acProcessTextCommand , "%T" ) ;
	if( p )
	{
		snprintf( cmd , sizeof(cmd)-1 , "%.*s%s%s" , (int)(p-g_stEditUltraMainConfig.acProcessTextCommand),g_stEditUltraMainConfig.acProcessTextCommand , acSelText , p+2 );
	}
	else
	{
		strcpy( cmd , g_stEditUltraMainConfig.acProcessTextCommand );
	}

	free( acSelText );

	STARTUPINFO si ;
	PROCESS_INFORMATION pi ;
	memset( & si , 0x00 , sizeof(STARTUPINFO) );
	si.cb = sizeof(STARTUPINFO) ;
	memset( & pi , 0x00 , sizeof(PROCESS_INFORMATION) );
	bret = CreateProcess( NULL , cmd , NULL , NULL , NULL , 0 , NULL , NULL , & si , & pi ) ;
	if( bret == TRUE )
		CloseHandle( pi.hProcess );

	return 0;
}
