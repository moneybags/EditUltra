#ifndef _H_EDITULTRA_SEARCH_
#define _H_EDITULTRA_SEARCH_

#include "framework.h"

struct NavigateBackNextTrace
{
	struct TabPage		*pnodeTabPage ;
	HWND			hwndScintilla ;
	int			nLastPos ;

	struct list_head	nodeNavigateBackNextTrace ;
};

extern HWND hwndSearchFind ;
extern HWND hwndEditBoxInSearchFind ;
extern HWND hwndSearchReplace ;
extern HWND hwndFromEditBoxInSearchReplace ;
extern HWND hwndToEditBoxInSearchReplace ;

void JumpGotoLine( struct TabPage *pnodeTabPage , int nGotoLineNo , int nCurrentLineNo );

INT_PTR CALLBACK SearchFindWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK SearchReplaceWndProc(HWND, UINT, WPARAM, LPARAM);

int OnSearchFind( struct TabPage *pnodeTabPage );
int OnSearchFindPrev( struct TabPage *pnodeTabPage );
int OnSearchFindNext( struct TabPage *pnodeTabPage );
int OnSearchReplace( struct TabPage *pnodeTabPage );
int OnSelectAll( struct TabPage *pnodeTabPage );
int OnSelectWord( struct TabPage *pnodeTabPage );
int OnSelectLine( struct TabPage *pnodeTabPage );
int OnAddSelectLeftCharGroup( struct TabPage *pnodeTabPage );
int OnAddSelectRightCharGroup( struct TabPage *pnodeTabPage );
int OnAddSelectLeftWord( struct TabPage *pnodeTabPage );
int OnAddSelectRightWord( struct TabPage *pnodeTabPage );
int OnAddSelectTopBlockFirstLine( struct TabPage *pnodeTabPage );
int OnAddSelectBottomBlockFirstLine( struct TabPage *pnodeTabPage );

int OnMoveLeftCharGroup( struct TabPage *pnodeTabPage );
int OnMoveRightCharGroup( struct TabPage *pnodeTabPage );
int OnMoveLeftWord( struct TabPage *pnodeTabPage );
int OnMoveRightWord( struct TabPage *pnodeTabPage );
int OnMoveTopBlockFirstLine( struct TabPage *pnodeTabPage );
int OnMoveBottomBlockFirstLine( struct TabPage *pnodeTabPage );
int OnSearchGotoLine( struct TabPage *pnodeTabPage );

int OnSearchToggleBookmark( struct TabPage *pnodeTabPage );
int OnSearchAddBookmark( struct TabPage *pnodeTabPage );
int OnSearchRemoveBookmark( struct TabPage *pnodeTabPage );
int OnSearchRemoveAllBookmarks( struct TabPage *pnodeTabPage );
int OnSearchGotoPrevBookmark( struct TabPage *pnodeTabPage );
int OnSearchGotoNextBookmark( struct TabPage *pnodeTabPage );
int OnSearchGotoPrevBookmarkInAllFiles( struct TabPage *pnodeTabPage );
int OnSearchGotoNextBookmarkInAllFiles( struct TabPage *pnodeTabPage );

void InitNavigateBackNextTraceList();
int AddNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos );
int UpdateNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos );
int OnSearchNavigateBackPrev_InThisFile();
int OnSearchNavigateBackPrev_InAllFiles();
void CleanNavigateBackNextTraceListByThisFile( struct TabPage *pnodeTabPage );

#endif
