#include "framework.h"

struct DatabaseLibraryFunctions		stDatabaseLibraryFunctions = { 0 } ;

struct RedisLibraryFunctions		stRedisLibraryFunctions = { 0 } ;

/*
 * 结果表格控件
 */

int CreateQueryResultTableCtl( struct TabPage *pnodeTabPage , HFONT hFont )
{
	if( pnodeTabPage->hwndQueryResultTable )
	{
		DestroyWindow( pnodeTabPage->hwndQueryResultTable );
	}

	/* 创建结果表格控件 */
	// pnodeTabPage->hwndQueryResultTable = ::CreateWindow( WC_LISTVIEW , NULL , WS_BORDER|WS_CHILD|LVS_REPORT|LVS_EDITLABELS , pnodeTabPage->rectQueryResultListView.left , pnodeTabPage->rectQueryResultListView.top , pnodeTabPage->rectQueryResultListView.right-pnodeTabPage->rectQueryResultListView.left , pnodeTabPage->rectQueryResultListView.bottom-pnodeTabPage->rectQueryResultListView.top , g_hwndMainClient , NULL , g_hAppInstance , NULL ) ; 
	pnodeTabPage->hwndQueryResultTable = ::CreateWindow( WC_LISTVIEW , NULL , WS_CHILD|LVS_REPORT|LVS_EDITLABELS , pnodeTabPage->rectQueryResultListView.left , pnodeTabPage->rectQueryResultListView.top , pnodeTabPage->rectQueryResultListView.right-pnodeTabPage->rectQueryResultListView.left , pnodeTabPage->rectQueryResultListView.bottom-pnodeTabPage->rectQueryResultListView.top , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
	if( pnodeTabPage->hwndQueryResultTable == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建结果表格控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( pnodeTabPage->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
	ListView_SetExtendedListViewStyle( pnodeTabPage->hwndQueryResultTable , LVS_EX_FULLROWSELECT );

	return 0;
}

/*
* SQL
*/

int ParseSqlFileConfigHeader( struct TabPage *pnodeTabPage )
{
	struct DatabaseConnectionConfig	stDatabaseConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stDatabaseConnectionConfig , 0x00 , sizeof(struct DatabaseConnectionConfig) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( pnodeTabPage , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_DATABASE_CONNECTION_CONFIG , sizeof(BEGIN_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_DATABASE_CONNECTION_CONFIG , sizeof(END_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stDatabaseConnectionConfig.dbtype[0] )
				{
					memcpy( & (pnodeTabPage->stDatabaseConnectionConfig) , & stDatabaseConnectionConfig , sizeof(struct DatabaseConnectionConfig) );
					if( stDatabaseConnectionConfig.dbpass[0] )
						stDatabaseConnectionConfig.bConfigDbPass = TRUE ;
					else
						stDatabaseConnectionConfig.bConfigDbPass = FALSE ;
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%s" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "DBTYPE" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbtype , acConfigValue , sizeof(stDatabaseConnectionConfig.dbtype)-1 );
			else if( strcmp( acConfigKey , "DBHOST" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbhost , acConfigValue , sizeof(stDatabaseConnectionConfig.dbhost)-1 );
			else if( strcmp( acConfigKey , "DBPORT" ) == 0 )
				stDatabaseConnectionConfig.dbport = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "DBUSER" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbuser , acConfigValue , sizeof(stDatabaseConnectionConfig.dbuser)-1 );
			else if( strcmp( acConfigKey , "DBPASS" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbpass , acConfigValue , sizeof(stDatabaseConnectionConfig.dbpass)-1 );
			else if( strcmp( acConfigKey , "DBNAME" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbname , acConfigValue , sizeof(stDatabaseConnectionConfig.dbname)-1 );
		}
	}

	return 0;
}

int GetOracleErrCode( struct TabPage *pnodeTabPage , OCIError *errhpp , int *pnErrorCode , char *pcErrorDesc , size_t nErrorDescBufsize )
{
	struct OracleFunctions	*pstOracleFunctions = & (stDatabaseLibraryFunctions.stOracleFunctions) ;
	sb4			errcode ;
	sword			swResult ;

	errcode = 0 ;
	pnErrorCode[0] = '\0' ;
	swResult = pstOracleFunctions->pfuncOCIErrorGet( (dvoid *)errhpp , (ub4)1 , (text *)NULL , & errcode , (ub1 *)pcErrorDesc, (ub4)nErrorDescBufsize , OCI_HTYPE_ERROR ) ;
	if( swResult != OCI_SUCCESS )
	{
		AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获得Oracle错误详细消息失败" );
		return -1;
	}

	if( pnErrorCode )
		*(pnErrorCode) = errcode ;

	return 0;
}

int ConnectToDatabase( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	if( pnodeTabPage->stDatabaseConnectionConfig.dbtype[0] == '\0' )
		return 1;

	if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "MySQL" ) == 0 )
	{
		struct MySqlFunctions	*pstMySqlFunctions = & (stDatabaseLibraryFunctions.stMysqlFunctions) ;
		struct MySqlHandles	*pstMySqlHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stMysqlHandles) ;

		if( pstMySqlFunctions->hmod_libmysql_dll == NULL )
		{
			pstMySqlFunctions->hmod_libmysql_dll = ::LoadLibrary( "libmysql.dll" ) ;
			if( pstMySqlFunctions->hmod_libmysql_dll == NULL )
			{
				::MessageBox(NULL, TEXT("不能装载libmysql.dll，请检查是否已安装MySQL以及系统环境变量PATH是否包含MySQL动态链接库文件目录"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			pstMySqlFunctions->pfunc_mysql_init = (func_mysql_init *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_init" ) ;
			pstMySqlFunctions->pfunc_mysql_real_connect = (func_mysql_real_connect *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_real_connect" ) ;
			pstMySqlFunctions->pfunc_mysql_set_character_set = (func_mysql_set_character_set *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_set_character_set" ) ;
			pstMySqlFunctions->pfunc_mysql_query = (func_mysql_query *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_query" ) ;
			pstMySqlFunctions->pfunc_mysql_affected_rows = (func_mysql_affected_rows *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_affected_rows" ) ;
			pstMySqlFunctions->pfunc_mysql_store_result = (func_mysql_store_result *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_store_result" ) ;
			pstMySqlFunctions->pfunc_mysql_num_fields = (func_mysql_num_fields *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_num_fields" ) ;
			pstMySqlFunctions->pfunc_mysql_fetch_field = (func_mysql_fetch_field *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_fetch_field" ) ;
			pstMySqlFunctions->pfunc_mysql_fetch_row = (func_mysql_fetch_row *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_fetch_row" ) ;
			pstMySqlFunctions->pfunc_mysql_free_result = (func_mysql_free_result *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_free_result" ) ;
			pstMySqlFunctions->pfunc_mysql_close = (func_mysql_close *)::GetProcAddress( pstMySqlFunctions->hmod_libmysql_dll , "mysql_close" ) ;
			if(	pstMySqlFunctions->pfunc_mysql_init == NULL
				|| pstMySqlFunctions->pfunc_mysql_real_connect == NULL
				|| pstMySqlFunctions->pfunc_mysql_set_character_set == NULL
				|| pstMySqlFunctions->pfunc_mysql_query == NULL
				|| pstMySqlFunctions->pfunc_mysql_affected_rows == NULL
				|| pstMySqlFunctions->pfunc_mysql_store_result == NULL
				|| pstMySqlFunctions->pfunc_mysql_num_fields == NULL
				|| pstMySqlFunctions->pfunc_mysql_fetch_field == NULL
				|| pstMySqlFunctions->pfunc_mysql_fetch_row == NULL
				|| pstMySqlFunctions->pfunc_mysql_free_result == NULL
				|| pstMySqlFunctions->pfunc_mysql_close == NULL
				)
			{
				::MessageBox(NULL, TEXT("不能定位函数符号在libmysql.dll"), TEXT("错误"), MB_ICONERROR | MB_OK);
				FreeLibrary( pstMySqlFunctions->hmod_libmysql_dll );
				return -1;
			}
		}

		if( pstMySqlHandles->mysql == NULL )
		{
			pstMySqlHandles->mysql = pstMySqlFunctions->pfunc_mysql_init( NULL ) ;
			if( pstMySqlHandles->mysql == NULL )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"不能创建MySQL对象" );
				return -1;
			}

			pnodeTabPage->bIsDatabaseConnected = FALSE ;
		}

		if( pnodeTabPage->bIsDatabaseConnected == FALSE )
		{
			if( pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] == '\0' )
			{
				nret = InputBox( g_hwndMainWindow , "请输入数据库用户密码：" , "输入窗口" , 0 , pnodeTabPage->stDatabaseConnectionConfig.dbpass , sizeof(pnodeTabPage->stDatabaseConnectionConfig.dbpass)-1 ) ;
				if( nret == IDOK )
				{
					if( pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] == '\0' )
					{
						AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"未输入密码" );
						pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
						return -1;
					}
				}
				else if( nret == IDCANCEL )
				{
					AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"输入密码被取消" );
					pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
					return -1;
				}
				else
				{
					::MessageBox( NULL , "输入窗口返回错误" , TEXT("输入窗口") , MB_ICONERROR | MB_OK );
					pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
					return -1;
				}
			}

			pstMySqlHandles->mysql = pstMySqlFunctions->pfunc_mysql_real_connect( pstMySqlHandles->mysql , pnodeTabPage->stDatabaseConnectionConfig.dbhost , pnodeTabPage->stDatabaseConnectionConfig.dbuser , pnodeTabPage->stDatabaseConnectionConfig.dbpass , pnodeTabPage->stDatabaseConnectionConfig.dbname , pnodeTabPage->stDatabaseConnectionConfig.dbport , NULL , 0 ) ;
			if( pstMySqlHandles->mysql == NULL )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"连接MySQL服务器[%s:%d]失败，用户名[%s]，数据库[%s]" , pnodeTabPage->stDatabaseConnectionConfig.dbhost , pnodeTabPage->stDatabaseConnectionConfig.dbport , pnodeTabPage->stDatabaseConnectionConfig.dbuser , pnodeTabPage->stDatabaseConnectionConfig.dbname );
				pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
				if( pnodeTabPage->stDatabaseConnectionConfig.bConfigDbPass == FALSE )
					pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] = '\0' ;
				return -1;
			}
			else
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"连接MySQL服务器[%s:%d]成功，用户名[%s]，数据库[%s]" , pnodeTabPage->stDatabaseConnectionConfig.dbhost , pnodeTabPage->stDatabaseConnectionConfig.dbport , pnodeTabPage->stDatabaseConnectionConfig.dbuser , pnodeTabPage->stDatabaseConnectionConfig.dbname );
			}

			nret = 0 ;
			if( pnodeTabPage->nCodePage == 65001 )
			{
				nret = pstMySqlFunctions->pfunc_mysql_set_character_set( pstMySqlHandles->mysql , (char*)"UTF8" ) ;
			}
			else if( pnodeTabPage->nCodePage == 936 )
			{
				nret = pstMySqlFunctions->pfunc_mysql_set_character_set( pstMySqlHandles->mysql , (char*)"GBK" ) ;
			}
			if( nret )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"不能设置MySQL客户端编码[%d]" , pnodeTabPage->nCodePage );
				pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
				return -1;
			}

			pnodeTabPage->bIsDatabaseConnected = TRUE ;

			return 1;
		}
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "Oracle" ) == 0 )
	{
		struct OracleFunctions	*pstOracleFunctions = & (stDatabaseLibraryFunctions.stOracleFunctions) ;
		struct OracleHandles	*pstOracleHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stOracleHandles) ;
		sword			swResult ;

		if( pstOracleFunctions->hmod_oci_dll == NULL )
		{
			pstOracleFunctions->hmod_oci_dll = ::LoadLibrary( "oci.dll" ) ;
			if( pstOracleFunctions->hmod_oci_dll == NULL )
			{
				::MessageBox(NULL, TEXT("不能装载oci.dll，请检查是否已安装Oracle以及系统环境变量PATH是否包含Oracle动态链接库文件目录"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			pstOracleFunctions->pfuncOCIEnvCreate = (funcOCIEnvCreate *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIEnvCreate" ) ;
			pstOracleFunctions->pfuncOCIHandleAlloc = (funcOCIHandleAlloc *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIHandleAlloc" ) ;
			pstOracleFunctions->pfuncOCIHandleFree = (funcOCIHandleFree *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIHandleFree" ) ;
			pstOracleFunctions->pfuncOCIServerAttach = (funcOCIServerAttach *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIServerAttach" ) ;
			pstOracleFunctions->pfuncOCIServerDetach = (funcOCIServerDetach *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIServerDetach" ) ;
			pstOracleFunctions->pfuncOCISessionBegin = (funcOCISessionBegin *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCISessionBegin" ) ;
			pstOracleFunctions->pfuncOCISessionEnd = (funcOCISessionEnd *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCISessionEnd" ) ;
			pstOracleFunctions->pfuncOCIAttrGet = (funcOCIAttrGet *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIAttrGet" ) ;
			pstOracleFunctions->pfuncOCIAttrSet = (funcOCIAttrSet *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIAttrSet" ) ;
			pstOracleFunctions->pfuncOCIParamGet = (funcOCIParamGet *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIParamGet" ) ;
			pstOracleFunctions->pfuncOCIParamSet = (funcOCIParamSet *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIParamSet" ) ;
			pstOracleFunctions->pfuncOCIStmtPrepare = (funcOCIStmtPrepare *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIStmtPrepare" ) ;
			pstOracleFunctions->pfuncOCIStmtPrepare2 = (funcOCIStmtPrepare2 *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIStmtPrepare2" ) ;
			pstOracleFunctions->pfuncOCIDefineByPos = (funcOCIDefineByPos *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIDefineByPos" ) ;
			pstOracleFunctions->pfuncOCIDefineByPos2 = (funcOCIDefineByPos2 *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIDefineByPos2" ) ;
			pstOracleFunctions->pfuncOCIStmtExecute = (funcOCIStmtExecute *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIStmtExecute" ) ;
			pstOracleFunctions->pfuncOCIStmtFetch = (funcOCIStmtFetch *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIStmtFetch" ) ;
			pstOracleFunctions->pfuncOCIStmtFetch2 = (funcOCIStmtFetch2 *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIStmtFetch2" ) ;
			pstOracleFunctions->pfuncOCIErrorGet = (funcOCIErrorGet *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCIErrorGet" ) ;
			pstOracleFunctions->pfuncOCITransStart = (funcOCITransStart *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCITransStart" ) ;
			pstOracleFunctions->pfuncOCITransCommit = (funcOCITransCommit *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCITransCommit" ) ;
			pstOracleFunctions->pfuncOCITransRollback = (funcOCITransRollback *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCITransRollback" ) ;
			pstOracleFunctions->pfuncOCITransDetach = (funcOCITransDetach *)::GetProcAddress( pstOracleFunctions->hmod_oci_dll , "OCITransDetach" ) ;
			if(	pstOracleFunctions->pfuncOCIEnvCreate == NULL
				|| pstOracleFunctions->pfuncOCIEnvCreate == NULL
				|| pstOracleFunctions->pfuncOCIHandleAlloc == NULL
				|| pstOracleFunctions->pfuncOCIHandleFree == NULL
				|| pstOracleFunctions->pfuncOCIServerAttach == NULL
				|| pstOracleFunctions->pfuncOCIServerDetach == NULL
				|| pstOracleFunctions->pfuncOCISessionBegin == NULL
				|| pstOracleFunctions->pfuncOCISessionEnd == NULL
				|| pstOracleFunctions->pfuncOCIAttrGet == NULL
				|| pstOracleFunctions->pfuncOCIAttrSet == NULL
				|| pstOracleFunctions->pfuncOCIParamGet == NULL
				|| pstOracleFunctions->pfuncOCIParamSet == NULL
				|| pstOracleFunctions->pfuncOCIStmtPrepare == NULL
				|| pstOracleFunctions->pfuncOCIStmtPrepare2 == NULL
				|| pstOracleFunctions->pfuncOCIDefineByPos == NULL
				|| pstOracleFunctions->pfuncOCIDefineByPos2 == NULL
				|| pstOracleFunctions->pfuncOCIStmtExecute == NULL
				|| pstOracleFunctions->pfuncOCIStmtFetch == NULL
				|| pstOracleFunctions->pfuncOCIStmtFetch2 == NULL
				|| pstOracleFunctions->pfuncOCIErrorGet == NULL
				|| pstOracleFunctions->pfuncOCITransStart == NULL
				|| pstOracleFunctions->pfuncOCITransCommit == NULL
				|| pstOracleFunctions->pfuncOCITransRollback == NULL
				|| pstOracleFunctions->pfuncOCITransDetach == NULL
				)
			{
				::MessageBox(NULL, TEXT("不能定位函数符号在oci.dll"), TEXT("错误"), MB_ICONERROR | MB_OK);
				FreeLibrary( stDatabaseLibraryFunctions.stMysqlFunctions.hmod_libmysql_dll );
				return -1;
			}
		}

		if( pstOracleHandles->envhpp == NULL )
		{
			swResult = pstOracleFunctions->pfuncOCIEnvCreate( & (pstOracleHandles->envhpp) , OCI_DEFAULT , NULL , NULL , NULL , NULL , 0 , NULL ) ;
			if( swResult != OCI_SUCCESS && swResult != OCI_SUCCESS_WITH_INFO )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"不能创建Oracle对象" );
				return -1;
			}

			pstOracleFunctions->pfuncOCIHandleAlloc((dvoid *)(pstOracleHandles->envhpp), (dvoid **)&(pstOracleHandles->errhpp), OCI_HTYPE_ERROR, (size_t)0, (dvoid **)0);

			pnodeTabPage->bIsDatabaseConnected = FALSE ;
		}

		if( pnodeTabPage->bIsDatabaseConnected == FALSE )
		{
			if( pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] == '\0' )
			{
				nret = InputBox( g_hwndMainWindow , "请输入数据库用户密码：" , "输入窗口" , 0 , pnodeTabPage->stDatabaseConnectionConfig.dbpass , sizeof(pnodeTabPage->stDatabaseConnectionConfig.dbpass)-1 ) ;
				if( nret == IDOK )
				{
					if( pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] == '\0' )
					{
						AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"未输入密码" );
						return -1;
					}
				}
				else if( nret == IDCANCEL )
				{
					AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"输入密码被取消" );
					return -1;
				}
				else
				{
					::MessageBox( NULL , "输入窗口返回错误" , TEXT("输入窗口") , MB_ICONERROR | MB_OK );
					return -1;
				}
			}

			pstOracleFunctions->pfuncOCIHandleAlloc((dvoid *)(pstOracleHandles->envhpp), (dvoid **)&(pstOracleHandles->servhpp), OCI_HTYPE_SERVER, (size_t)0, (dvoid **)0);

			swResult = pstOracleFunctions->pfuncOCIServerAttach( pstOracleHandles->servhpp , pstOracleHandles->errhpp , (text *)pnodeTabPage->stDatabaseConnectionConfig.dbhost , (sb4)strlen(pnodeTabPage->stDatabaseConnectionConfig.dbhost) , 0 ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"连接Oracle服务器[%s]失败[%d][%s]" , pnodeTabPage->stDatabaseConnectionConfig.dbhost , nErrorCode , acErrorDesc );
				pstOracleFunctions->pfuncOCIHandleFree((dvoid *)(pstOracleHandles->servhpp) , OCI_HTYPE_SERVER ); pstOracleHandles->servhpp = NULL ;
				if( pnodeTabPage->stDatabaseConnectionConfig.bConfigDbPass == FALSE )
					pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] = '\0' ;
				return -1;
			}
			else
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"连接Oracle服务器[%s]成功" , pnodeTabPage->stDatabaseConnectionConfig.dbhost );
			}

			pstOracleFunctions->pfuncOCIHandleAlloc((dvoid *)(pstOracleHandles->envhpp), (dvoid **)&(pstOracleHandles->svchpp), OCI_HTYPE_SVCCTX, (size_t)0, (dvoid **)0);

			pstOracleFunctions->pfuncOCIAttrSet((dvoid *)(pstOracleHandles->svchpp) , OCI_HTYPE_SVCCTX , (dvoid *)(pstOracleHandles->servhpp) , (ub4)0 , OCI_ATTR_SERVER , (OCIError *)(pstOracleHandles->errhpp) );

			pstOracleFunctions->pfuncOCIHandleAlloc((dvoid *)(pstOracleHandles->envhpp) , (dvoid **)&(pstOracleHandles->usrhpp) , (ub4)OCI_HTYPE_SESSION , (size_t)0 , (dvoid **)0 );

			pstOracleFunctions->pfuncOCIAttrSet((dvoid *)(pstOracleHandles->usrhpp) , (ub4)OCI_HTYPE_SESSION , (dvoid *)(pnodeTabPage->stDatabaseConnectionConfig.dbuser), (ub4)strlen(pnodeTabPage->stDatabaseConnectionConfig.dbuser) , (ub4)OCI_ATTR_USERNAME , pstOracleHandles->errhpp );
			pstOracleFunctions->pfuncOCIAttrSet((dvoid *)(pstOracleHandles->usrhpp) , (ub4)OCI_HTYPE_SESSION , (dvoid *)(pnodeTabPage->stDatabaseConnectionConfig.dbpass), (ub4)strlen(pnodeTabPage->stDatabaseConnectionConfig.dbpass) , (ub4)OCI_ATTR_PASSWORD , pstOracleHandles->errhpp );

			swResult = pstOracleFunctions->pfuncOCISessionBegin( pstOracleHandles->svchpp , pstOracleHandles->errhpp , pstOracleHandles->usrhpp , OCI_CRED_RDBMS , (ub4)OCI_DEFAULT ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"创建Oracle用户会话失败[%d][%s]，用户名[%s]" , nErrorCode , acErrorDesc , pnodeTabPage->stDatabaseConnectionConfig.dbuser );
				pstOracleFunctions->pfuncOCIHandleFree((dvoid *)(pstOracleHandles->servhpp) , OCI_HTYPE_SERVER ); pstOracleHandles->servhpp = NULL ;
				pstOracleFunctions->pfuncOCIHandleFree((dvoid *)(pstOracleHandles->usrhpp) , OCI_HTYPE_SESSION ); pstOracleHandles->usrhpp = NULL ;
				if( pnodeTabPage->stDatabaseConnectionConfig.bConfigDbPass == FALSE )
					pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] = '\0' ;
				return -1;
			}
			else
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"创建Oracle用户会话成功，用户名[%s]" , pnodeTabPage->stDatabaseConnectionConfig.dbuser );
			}

			pstOracleFunctions->pfuncOCIAttrSet((dvoid *)(pstOracleHandles->svchpp) , (ub4) OCI_HTYPE_SVCCTX , (dvoid *)(pstOracleHandles->usrhpp) , (ub4)0 , (ub4)OCI_ATTR_SESSION , pstOracleHandles->errhpp );

			pnodeTabPage->bIsDatabaseConnected = TRUE ;

			return 1;
		}
	}
	else
	{
		AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"数据库类型[%s]暂不支持" , pnodeTabPage->stDatabaseConnectionConfig.dbtype );
	}

	return 0;
}

int ExecuteSqlQuery( struct TabPage *pnodeTabPage )
{
	int		nSelStartPos ;
	int		nSelEndPos ;
	int		nSelSqlLength ;
	char		*acSelSql = NULL ;

	HWND		hwndListViewHeader ;
	int		nFieldCount ;
	int		nFieldIndex ;
	int		nRowIndex ;
	unsigned int	*anFieldWidth = NULL ;
	unsigned int	nFieldWidth ;
	LVCOLUMN	lvc ;
	LVITEM		lvi ;

	int		nret = 0 ;

	nret = ConnectToDatabase( pnodeTabPage ) ;
	if( nret < 0 )
	{
		return nret;
	}
	else if( nret == 1 )
	{
		if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
		{
			nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( pnodeTabPage ) ;
			if( nret )
			{
				return nret;
			}
		}
	}

	nSelStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	nSelEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	nSelSqlLength = nSelEndPos - nSelStartPos ;
	if( nSelSqlLength <= 0 )
		return 0;

	acSelSql = (char*)malloc( nSelSqlLength+1 ) ;
	if( acSelSql == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放SQL"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acSelSql , 0x00 , nSelSqlLength+1 );
	GetTextByRange( pnodeTabPage , nSelStartPos , nSelEndPos , acSelSql );

	AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL[%s]" , acSelSql );

	if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "MySQL" ) == 0 )
	{
		struct MySqlFunctions	*pstMySqlFunctions = & (stDatabaseLibraryFunctions.stMysqlFunctions) ;
		struct MySqlHandles	*pstMySqlHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stMysqlHandles) ;
		MYSQL_RES	*stMysqlResult = NULL ;

		nret = pstMySqlFunctions->pfunc_mysql_query( pstMySqlHandles->mysql , acSelSql ) ;
		free( acSelSql );
		if( nret )
		{
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL失败[%d]" , nret );
			pstMySqlFunctions->pfunc_mysql_close( pstMySqlHandles->mysql ); pstMySqlHandles->mysql = NULL ;
			return -1;
		}
		else
		{
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL成功，受影响记录数量[%d]" , pstMySqlFunctions->pfunc_mysql_affected_rows(pstMySqlHandles->mysql) );
		}

		stMysqlResult = pstMySqlFunctions->pfunc_mysql_store_result( pstMySqlHandles->mysql ) ;
		if( stMysqlResult )
		{
			MYSQL_FIELD	*pstMysqlField = NULL ;
			MYSQL_ROW	stMysqlRow ;

			ListView_DeleteAllItems( pnodeTabPage->hwndQueryResultTable );
			hwndListViewHeader = ListView_GetHeader( pnodeTabPage->hwndQueryResultTable ) ;
			nFieldCount = (int)::SendMessage( hwndListViewHeader , HDM_GETITEMCOUNT , 0 , 0 ) ;
			for( ; nFieldCount >= 0 ; nFieldCount-- )
			{
				ListView_DeleteColumn( pnodeTabPage->hwndQueryResultTable , nFieldCount );
			}

			nFieldCount = pstMySqlFunctions->pfunc_mysql_num_fields(stMysqlResult) ;
			anFieldWidth = (unsigned int *)malloc( sizeof(unsigned int) * nFieldCount ) ;
			if( anFieldWidth == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存以存放所有字段宽度"), TEXT("错误"), MB_ICONERROR | MB_OK);
				pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
				return -1;
			}
			memset( anFieldWidth , 0x00 , sizeof(unsigned int) * nFieldCount );

			for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
			{
				pstMysqlField = pstMySqlFunctions->pfunc_mysql_fetch_field(stMysqlResult) ;

				memset( & lvc , 0x00 , sizeof(LVCOLUMN) );
				lvc.mask = LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_FMT ;
				lvc.iSubItem = nFieldIndex ;
				lvc.pszText = pstMysqlField->name ;
				lvc.cx = 100 ;
				lvc.fmt = LVCFMT_CENTER ;
				nret = ListView_InsertColumn( pnodeTabPage->hwndQueryResultTable , nFieldIndex , & lvc ) ;
				if( nret == -1 )
				{
					::MessageBox(NULL, TEXT("不能插入列表视图的头部列名"), TEXT("错误"), MB_ICONERROR | MB_OK);
					pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
					free( anFieldWidth );
					return -1;
				}

				nFieldWidth = ((int)strlen(pstMysqlField->name)+2) * CHAR_WIDTH ;
				if( anFieldWidth[nFieldIndex] < nFieldWidth )
					anFieldWidth[nFieldIndex] = nFieldWidth ;
			}

			nRowIndex = 0 ;
			while( ( stMysqlRow = pstMySqlFunctions->pfunc_mysql_fetch_row(stMysqlResult) ) )
			{
				memset( & lvi , 0x00 , sizeof(LVITEM) );
				lvi.mask = LVIF_TEXT | LVIF_STATE ;
				lvi.iItem = nRowIndex ;
				lvi.stateMask = 0 ;
				lvi.state = 0 ;

				for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
				{
					if( nFieldIndex == 0 )
					{
						lvi.iSubItem = nFieldIndex ;
						lvi.pszText = stMysqlRow[nFieldIndex] ;
						nret = ListView_InsertItem( pnodeTabPage->hwndQueryResultTable , & lvi ) ;
					}
					else
					{
						ListView_SetItemText( pnodeTabPage->hwndQueryResultTable , nRowIndex , nFieldIndex , stMysqlRow[nFieldIndex] );
					}
					if( nret == -1 )
					{
						::MessageBox(NULL, TEXT("不能插入列表视图的记录"), TEXT("错误"), MB_ICONERROR | MB_OK);
						pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
						free( anFieldWidth );
						return -1;
					}

					if( stMysqlRow[nFieldIndex] )
					{
						nFieldWidth = ((int)strlen(stMysqlRow[nFieldIndex])+2) * CHAR_WIDTH ;
						if( anFieldWidth[nFieldIndex] < nFieldWidth )
							anFieldWidth[nFieldIndex] = nFieldWidth ;
					}
				}

				nRowIndex++;
			}

			for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
			{
				ListView_SetColumnWidth( pnodeTabPage->hwndQueryResultTable , nFieldIndex , anFieldWidth[nFieldIndex] );
			}

			pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
			free( anFieldWidth );
		}

		return 0;
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "Oracle" ) == 0 )
	{
		struct OracleFunctions	*pstOracleFunctions = & (stDatabaseLibraryFunctions.stOracleFunctions) ;
		struct OracleHandles	*pstOracleHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stOracleHandles) ;
		sword			swResult ;

		OCIStmt		*stmthpp = NULL ;
		ub2		stmt_type ;
		OCITrans	*txnhpp = NULL ;
		OCIParam	*paramhpp = NULL ;
		text		*column_name = NULL ;
		ub4		column_name_len = 0 ;
		OCIDefine	*column_ocid = NULL ;
		struct ColumnData
		{
			char	data[ 4000 + 1 ] ;
			ub2	data_len ;
			sb2	data_indicator ;
		} *astColumnData ;

		pstOracleFunctions->pfuncOCIHandleAlloc( (dvoid *)(pstOracleHandles->envhpp) , (dvoid **) & stmthpp , OCI_HTYPE_STMT , (size_t)0 , (dvoid **)0 );
		swResult = pstOracleFunctions->pfuncOCIStmtPrepare( stmthpp , pstOracleHandles->errhpp , (text *)acSelSql , (ub4)strlen(acSelSql) , (ub4)OCI_NTV_SYNTAX , (ub4)OCI_DEFAULT ) ;
		free( acSelSql );
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"构造SQL失败[%d][%s]" , nErrorCode , acErrorDesc );
			return -1;
		}

		pstOracleFunctions->pfuncOCIAttrGet( stmthpp , OCI_HTYPE_STMT , & stmt_type , NULL , OCI_ATTR_STMT_TYPE , pstOracleHandles->errhpp);

		if( stmt_type != OCI_STMT_SELECT )
		{
			pstOracleFunctions->pfuncOCIHandleAlloc((void *)(pstOracleHandles->envhpp) , (void **) & txnhpp , OCI_HTYPE_TRANS , 0 , 0 );
			pstOracleFunctions->pfuncOCIAttrSet( (void *)(pstOracleHandles->svchpp) , OCI_HTYPE_SVCCTX , (void *) txnhpp , 0 , OCI_ATTR_TRANS , pstOracleHandles->errhpp );

			pstOracleFunctions->pfuncOCITransStart( pstOracleHandles->svchpp , pstOracleHandles->errhpp , 10 , OCI_TRANS_NEW );
		}

		swResult = pstOracleFunctions->pfuncOCIStmtExecute( pstOracleHandles->svchpp , stmthpp , pstOracleHandles->errhpp , (ub4)(stmt_type==OCI_STMT_SELECT?0:1) , (ub4)0 , (OCISnapshot *)NULL , (OCISnapshot *)NULL , OCI_DEFAULT ) ;
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL失败[%d][%s]" , nErrorCode , acErrorDesc );
			if( stmt_type != OCI_STMT_SELECT )
			{
				pstOracleFunctions->pfuncOCITransRollback( pstOracleHandles->svchpp , pstOracleHandles->errhpp , OCI_DEFAULT );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"回滚数据库事务" );
			}
			return -1;
		}
		else
		{
			if( stmt_type != OCI_STMT_SELECT )
			{
				pstOracleFunctions->pfuncOCITransCommit( pstOracleHandles->svchpp , pstOracleHandles->errhpp , OCI_DEFAULT );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"提交数据库事务" );
				pstOracleFunctions->pfuncOCIHandleFree( (void*)txnhpp , OCI_HTYPE_TRANS );
			}
		}

		if( stmt_type == OCI_STMT_SELECT )
		{
			ListView_DeleteAllItems( pnodeTabPage->hwndQueryResultTable );
			hwndListViewHeader = ListView_GetHeader( pnodeTabPage->hwndQueryResultTable ) ;
			nFieldCount = (int)::SendMessage( hwndListViewHeader , HDM_GETITEMCOUNT , 0 , 0 ) ;
			for( ; nFieldCount >= 0 ; nFieldCount-- )
			{
				ListView_DeleteColumn( pnodeTabPage->hwndQueryResultTable , nFieldCount );
			}

			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL成功" );

			ub4 nFieldCount2 ;
			pstOracleFunctions->pfuncOCIAttrGet( (dvoid *)stmthpp , OCI_HTYPE_STMT , & nFieldCount2 , 0 , OCI_ATTR_PARAM_COUNT , pstOracleHandles->errhpp );
			nFieldCount = (int)nFieldCount2 ;

			anFieldWidth = (unsigned int *)malloc( sizeof(unsigned int) * nFieldCount ) ;
			if( anFieldWidth == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存以存放所有字段宽度"), TEXT("错误"), MB_ICONERROR | MB_OK);
				pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
				return -1;
			}
			memset( anFieldWidth , 0x00 , sizeof(unsigned int) * nFieldCount );

			astColumnData = (struct ColumnData *)malloc( sizeof(struct ColumnData) * nFieldCount ) ;
			if( astColumnData == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存以存放所有字段值缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
				pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
				free( anFieldWidth );
				return -1;
			}
			memset( astColumnData , 0x00 , sizeof(struct ColumnData) * nFieldCount );

			for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
			{
				swResult = pstOracleFunctions->pfuncOCIParamGet( stmthpp , OCI_HTYPE_STMT , pstOracleHandles->errhpp , (dvoid **) & paramhpp , nFieldIndex+1 ) ;
				if( swResult != OCI_SUCCESS )
				{
					int	nErrorCode ;
					char	acErrorDesc[ 512 ] = "" ;
					GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
					AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获得字段[%d]名失败[%d][%s]" , nFieldIndex , nErrorCode , acErrorDesc );
					pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
					free( anFieldWidth );
					free( astColumnData );
					return -1;
				}

				pstOracleFunctions->pfuncOCIAttrGet( (dvoid *)paramhpp , OCI_DTYPE_PARAM , (dvoid *) & column_name , & column_name_len , OCI_ATTR_NAME , pstOracleHandles->errhpp );

				memset( & lvc , 0x00 , sizeof(LVCOLUMN) );
				lvc.mask = LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_FMT ;
				lvc.iSubItem = nFieldIndex ;
				lvc.pszText = (char*)column_name ;
				lvc.cx = 100 ;
				lvc.fmt = LVCFMT_CENTER ;
				nret = ListView_InsertColumn( pnodeTabPage->hwndQueryResultTable , nFieldIndex , & lvc ) ;
				if( nret == -1 )
				{
					::MessageBox(NULL, TEXT("不能插入列表视图的头部列名"), TEXT("错误"), MB_ICONERROR | MB_OK);
					pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
					free( anFieldWidth );
					free( astColumnData );
					return -1;
				}

				pstOracleFunctions->pfuncOCIDefineByPos( stmthpp , & column_ocid , pstOracleHandles->errhpp , nFieldIndex+1 , (dvoid *)(astColumnData[nFieldIndex].data) , sizeof(astColumnData[nFieldIndex].data)-1 , SQLT_STR , (void*)&(astColumnData[nFieldIndex].data_indicator) , & (astColumnData[nFieldIndex].data_len) , NULL , OCI_DEFAULT );

				nFieldWidth = ((int)column_name_len+2) * CHAR_WIDTH ;
				if( anFieldWidth[nFieldIndex] < nFieldWidth )
					anFieldWidth[nFieldIndex] = nFieldWidth ;
			}

			nRowIndex = 0 ;
			while(1)
			{
				memset( & lvi , 0x00 , sizeof(LVITEM) );
				lvi.mask = LVIF_TEXT | LVIF_STATE ;
				lvi.iItem = nRowIndex ;
				lvi.stateMask = 0 ;
				lvi.state = 0 ;

				swResult = pstOracleFunctions->pfuncOCIStmtFetch2( stmthpp , pstOracleHandles->errhpp , 1 , OCI_FETCH_NEXT , 1 , OCI_DEFAULT ) ;
				if( swResult == OCI_NO_DATA )
				{
					break;
				}
				else if( swResult != OCI_SUCCESS )
				{
					pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
					free( anFieldWidth );
					free( astColumnData );
					return -1;
				}

				for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
				{
					if( nFieldIndex == 0 )
					{
						lvi.iSubItem = nFieldIndex ;
						lvi.pszText = astColumnData[nFieldIndex].data ;
						nret = ListView_InsertItem( pnodeTabPage->hwndQueryResultTable , & lvi ) ;
					}
					else
					{
						ListView_SetItemText( pnodeTabPage->hwndQueryResultTable , nRowIndex , nFieldIndex , astColumnData[nFieldIndex].data );
					}
					if( nret == -1 )
					{
						::MessageBox(NULL, TEXT("不能插入列表视图的记录"), TEXT("错误"), MB_ICONERROR | MB_OK);
						pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
						free( anFieldWidth );
						free( astColumnData );
						return -1;
					}

					if( astColumnData[nFieldIndex].data_len > 0 )
					{
						nFieldWidth = ((int)(astColumnData[nFieldIndex].data_len)+2) * CHAR_WIDTH ;
						if( anFieldWidth[nFieldIndex] < nFieldWidth )
							anFieldWidth[nFieldIndex] = nFieldWidth ;
					}
				}

				nRowIndex++;
			}

			for( nFieldIndex = 0 ; nFieldIndex < nFieldCount ; nFieldIndex++ )
			{
				ListView_SetColumnWidth( pnodeTabPage->hwndQueryResultTable , nFieldIndex , anFieldWidth[nFieldIndex] );
			}

			free( anFieldWidth );
			free( astColumnData );
		}
		else
		{
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行SQL成功" );
		}

		pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );

		return 0;
	}
	else
	{
		AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"数据库类型[%s]暂不支持" , pnodeTabPage->stDatabaseConnectionConfig.dbtype );
		return 0;
	}
}

int ParseRedisFileConfigHeader( struct TabPage *pnodeTabPage )
{
	struct RedisConnectionConfig	stRedisConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stRedisConnectionConfig , 0x00 , sizeof(struct RedisConnectionConfig) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( pnodeTabPage , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_REDIS_CONNECTION_CONFIG , sizeof(BEGIN_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_REDIS_CONNECTION_CONFIG , sizeof(END_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stRedisConnectionConfig.host[0] )
				{
					memcpy( & (pnodeTabPage->stRedisConnectionConfig) , & stRedisConnectionConfig , sizeof(struct RedisConnectionConfig) );
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%s" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "HOST" ) == 0 )
				strncpy( stRedisConnectionConfig.host , acConfigValue , sizeof(stRedisConnectionConfig.host)-1 );
			else if( strcmp( acConfigKey , "PORT" ) == 0 )
				stRedisConnectionConfig.port = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "PASS" ) == 0 )
				strncpy( stRedisConnectionConfig.pass , acConfigValue , sizeof(stRedisConnectionConfig.pass)-1 );
			else if( strcmp( acConfigKey , "DBSL" ) == 0 )
				strncpy( stRedisConnectionConfig.dbsl , acConfigValue , sizeof(stRedisConnectionConfig.dbsl)-1 );
		}
	}

	return 0;
}
